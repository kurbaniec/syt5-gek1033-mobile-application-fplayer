<h1 align="center">
  <br>
  <img src="images/logo/fplayer-logo-alt.png" alt="fplayer" width="250"></a>
  <br>
  fplayer
  <br>
</h1>

<h4 align="center">The music player that syncs.</h4>
## 🎶 About

Fplayer  is a offline-centric music player that connects to your Google Drive to access to your music library. At the initial setup you can choose the folder where all your music is stored, which is then replicated onto your device. When you add or remove songs in your Drive,  fplayer will automatically add, update or delete them in the app.  Fplayer uses internally an [SQLite](https://flutter.dev/docs/cookbook/persistence/sqlite) database to manage your songs quickly and reliably.

The app allows to create playlists for your favourite songs. All playlists information are uploaded to Google Drive, so that you can have access to all your playlists on multiple devices.

## :arrows_counterclockwise: Synchronization approach

Fplayer uses the [Google Drive API v3](https://developers.google.com/drive/api/v3/reference) to retrieve file changes trough the changes interface. The synchronization is polling-based, that means that the app sends requests in a set interval to the Google Drive endpoint to retrieve changes. Each requests sends a token, which is used to identify the current state. When no new changes are found, the token stays the same, when some changes are found the token is replaced through a new one, that is saved in the internal database of the app. Found changes are iterated over in a loop, that checks if the changed file is part of the music library and if the file got added, changed or deleted. 

To keep the playlist configuration consistent, the configuration is uploaded to Google Drive when a playlist change occurs - like adding a new playlist. When a  different device makes a change to playlists, the synchronization system will detect it through a new change and build a lossless merge of the local and changed configuration and upload it. If the local and changed configuration are identical, no upload is needed.

A polling-based synchronization can consume quite a lot of mobile data, therefore fplayer allows you to change the synchronization interval in the settings page. On default, it is set to a periodic check every 60 seconds. If you want, you can disable this feature completely and use the manual synchronization found at the top of the settings page.

<p align="middle">
  <img src="images/readme/app/settings.jpg" width="200" />
  <img src="images/readme/app/settings_change_interval.jpg" width="200" /> 
</p>


## :wrench: Setup

You can install fplayer by obtaining the latest release on the GitHub page. The app is provided as an `apk` for easy installation on Android. The iOS support of the app is currently untested and not provided. If you want to build the app by oneself check the next section which goes into more detail about this.

When you start the app, you will see login button. Press it and you can login with your Google account to provide the app the access to your Google Drive. The app does not track nor sends your personal data anywhere, only the information that someone has logged in is tracked in the Firebase project that is needed to provide Google Sign for the app.

  <p align="middle">
  <img src="images/readme/app/login_screen.jpg" width="200" />
  <img src="images/readme/app/login_set_account.jpg" width="200" /> 
</p>

 After the login you will be prompted to select the location of your music folder. This a simplified representation of your Drive. 

 <p align="middle">
  <img src="images/readme/drive/drive_overview.PNG" width="300" />
  <img src="images/readme/app/login_select_location.jpg" width="200" /> 
  <img src="images/readme/app/login_confirm.jpg" width="200" />
</p>

After confirming your changes, the download for all of your current songs will start. This can take a while, depending on your internet connetion and size of your music library. Please do not close the app or put your phone to sleep during the process. During the process the playlist configuration will be uploaded to your Drive folder.

 <p align="middle">
  <img src="images/readme/drive/drive_config.PNG" width="400" />
  <img src="images/readme/app/login_wait.jpg" width="200" /> 
</p>

After the process has finished you should be greeted by the home screen. Now you can play your music, even without internet connection.

  <p align="middle">
  <img src="images/readme/app/home.jpg" width="200" />
  <img src="images/readme/app/playback.jpg" width="200" /> 
</p>

The app has three main tabs, "All songs" where every song is displayed, "Playlists" where your own playlists are found and "Albums", which show all your albums generated through the metadata of your songs. To manage playlists tap on the "three dots" icon of a song to see an options dialog where you can create playlists and add songs to them.

  <p align="middle">
  <img src="images/readme/app/options.jpg" width="200" />
  <img src="images/readme/app/create_playlist.jpg" width="200" /> 
  <img src="images/readme/app/add_song_to_playlist.jpg" width="200" />
  <img src="images/readme/app/playlists.jpg" width="200" />
</p>

Now you should have learned everything to master fplayer. Enjoy your music!

## :hammer_and_wrench: Build the app

Before you can build the app, you need to configure your own Firebase project that handles Google authentication for your built fplayer app. You cannot use the existing configuration (`google-services.json`) found in this repo because it would lead to failed login attempts, because firebase would not accept the signature of the app. 

All relevant information on how to configure Firebase can be found in the [documentation](https://pub.dev/packages/google_sign_in) of the `google_sign_in` package.

### Build Android version

To build an Android `apk` run the following commands.

```
flutter clean
flutter build apk
```

The building process can take a few minutes. When you have your device connected, you can run `flutter install` to install the built `apk` on your device.

### Build iOS version

iOS support for fplayer is not tested. If you want to try it out, check the official Flutter [documentation](https://flutter.dev/docs/deployment/ios) on how to build the iOS version.  

## :heavy_plus_sign: More...

### Userstories

Link to development [Userstories](UserstoryList.md).

### Flutter Cheat Sheet

Link to Flutter [Quickstart](flutter.md).

### Descriptions

Link to [API](api.md) description 

Link to [architecture](architecture.md) description

Link to [synchronisation](sync.md) description

## :books: ​Sources

* [Googleapis example project | 28.02.2020](https://engineering.nodesagency.com/categories/android/ios/2019/08/21/flutter-google-docs)
* [Googleapis package | 28.02.2020](https://pub.dev/packages/googleapis)
* [Google_sign_in package | 28.02.2020](https://pub.dev/packages/google_sign_in)
* [Google Drive API | 29.02.2020](https://developers.google.com/drive/api/v3/reference)
* [Google Drive API Retrieve Changes | 16.04.202](https://developers.google.com/drive/api/v3/manage-changes)