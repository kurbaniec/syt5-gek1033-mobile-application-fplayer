## Beschreibung der Schnittstellen

### Google Sign In

In diesem Projekt wurde, wie schon in der Architektur beschrieben, Google Sign In verwendet. Damit ist man in der Lage mit einem einmaligem Login die App zu verwenden und auf die eigene Drive-Ablage zuzugreifen. Man muss davor die URL definieren auf die man zugreifen möchte:

Man braucht das folgende Flutter-Plugin:

```dart
import 'package:google_sign_in/google_sign_in.dart';
```

Sowie das Package im Projekt:

``` dart
dependencies:
	google_sign_in:
```

 [^1]

```dart
GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: <String>[
      'email',
      'https://www.googleapis.com/auth/drive'
	],
);
```

**Einloggen des Benutzers:**

```dart
Future<void> login() async {
    _account = await _googleSignIn.signIn();
}
```

Der Ausdruck `await` ermöglicht es die Methode Asynchron aufzurufen und somit an der aufgerufenen Stelle des Programms zu blockieren. [^2]

Der Ausdruck `async` definiert die Methode als Asynchron, das bedeutet dass die Methode als Prozess läuft und das Programm einfach weitermacht an der Stelle. Dabei spielt der Datentyp `Future` eine große Rolle. Future definiert einen potentiellen zukünftigen Wert, das bedeutet dass eine Funktion einen Wert in der Zukunft zurückliefen kann. 

### SQLLITE Datenbank

Wie schon in der Architektur erklärt ist SQLlite eine relationale Datenbank, die ohne Serversoftware auskommt und alles in Dateien abspeichert. SQLlite wird verwendet mit einem Plugin in Flutter um Daten von **Songs**, **Playlist**,... zu speichern. Die Tabellen sind in der [Architektur](architecture.md) beschrieben. Um das verwenden zu können müssen die notwendigen Plugins und Packages vorhanden sein (siehe [Architektur](architecture.md)). Folgende Schritte sind notwendig um die Datenbank in Flutter zu verwenden:

Zunächst muss das jeweilige Datenmodel erstellt werden, in Flutter wird die mittels einer Klasse gemacht. Hier zum Beispiel die Klasse Playlist:

```dart
class Playlist implements{
  int id;
  String title;
  String songIds;
  List<Song> songs = List();

  Playlist({this.id, this.title, this.songIds});
}
```

Bevor man Daten schreiben und lesen kann, muss man sich mit der Datenbank verbinden. Dabei sind 2 Schritte notwendig:

1. Erstellen des Pfads zur Datenbankdatei mittels `getDatabasesPath()` vom **sqflite** Package zusammen mit der `join()` Funktion vom **Path** Package. 
2. Danach muss man die Datenbank öffnen mit der Funktion  `openDatabase()` function vom **sqflite** Package 

```dart
final Future<Database> database = openDatabase(
  join(await getDatabasesPath(), '.fplayer.config'),
);
```

Danach kann man eine Tabelle mit den gewünschten Daten erstellen. Hierbei werden einfache SQL-Befehle mittels `execute()` ausgeführt.

```dart
_driveDB = await openDatabase(
      _configPath,
      onCreate: (db, version) {
        db.execute(
          "CREATE TABLE Playlist("
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "title TEXT,"
            "songids TEXT"
          ")"
        );
      },
      version: 1
    );
```



 

 [^3]



## Quellen

[^1]: [Google Sign In Package](https://pub.dev/packages/google_sign_in)
[^2]: [Async-await](https://dart.dev/codelabs/async-await)
[^ 3]: [SQLlite](https://flutter.dev/docs/cookbook/persistence/sqlite)

