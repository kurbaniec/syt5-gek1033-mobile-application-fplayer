# Working with Flutter

## Install

The Flutter SDK can be installed via git:

```
git clone https://github.com/flutter/flutter.git -b stable
```

After cloning the repository containing the SDK update your path so you can use the flutter commands.

To test if the installation was successful, run the following command, which should return an output without errors:

```
flutter doctor
```

## Android setup

### Android Studio

First download and install [Android Studio](https://developer.android.com/studio). Proceed with the installation wizard, which installs the Android SDK. If you are a IntelliJ user, do NOT install Android Studio, install the Android plugin instead. Android Studio is based on IntelliJ! 

Next install the plugins for Dart and Flutter to finish the IDE setup.

### Device Setup

It is recommended to use your own Android phone to run and test your Flutter app. 

The setup contains following steps:

1. Enable **Developer options** and **USB debugging** on your device. Detailed instructions are available in the [Android documentation](https://developer.android.com/studio/debug/dev-options).
2. Windows-only: Install the [Google USB Driver](https://developer.android.com/studio/run/win-usb).
3. Using a USB cable, plug your phone into your computer. If prompted on your device, authorize your computer to access your device.
4. In the terminal, run the `flutter devices` command to verify that Flutter recognizes your connected Android device. By default, Flutter uses the version of the Android SDK where your `adb` tool is based. If you want Flutter to use a different installation of the Android SDK, you must set the `ANDROID_HOME` environment variable to that installation directory.

If you do not own a Android phone or want to test compatibility with a newer/older Android version, you need to setup the Android Emulator. For this, checkout the official [setup instructions](https://flutter.dev/docs/get-started/install/windows#set-up-the-android-emulator).

## Create a new app

When working with Android Studio / IntelliJ, you should see a flutter as an option for new projects:

![](images/project.PNG)

Use this functionality to create a new Flutter App with ease.

## Run the app

The toolbar is your place to go for running and debugging your application:

![](images/run.PNG)

The Target selector is your physical or virtual device, where the app will be installed and run.

With the debug option you can add breakpoints in your code, that will be triggered when you do an action on your device and hold the progress of the app. 

Hot reload is an awesome feature of Flutter. When you make changes, you can easily reload the app on your device with the new changes.

## Sources

* [Get started with Flutter | 20.20.200](https://flutter.dev/docs/get-started/install/windows)