package com.fplayer

import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class MainActivity: FlutterActivity() {
    private val CHANNEL  = "audioMetaData"

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler {
            call, result ->
            when(call.method) {
                "readTags" -> {
                    val filePath = call.argument<String>("filePath")!!
                    val coverPath = call.argument<String>("coverPath")!!

                    val tags = readTags(filePath, coverPath)
                    result.success(tags)
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }

    // Source: https://stackoverflow.com/a/33360379/12347616
    private fun readTags(filePath: String, coverPath: String): List<Any> {

        val file = java.io.FileInputStream(filePath)

        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(file.fd)

        var title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)
        if (title == null) title = "Unknown title"
        var artist = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)
        if (artist == null) artist = "Unknown artist"
        var duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
        if (duration == null) duration = "0"
        var album = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM)
        if (album == null) album = "Unknown album"
        val cover = retriever.embeddedPicture
        var hasCover = false
        if (cover != null) {
            val bitmap = android.graphics.BitmapFactory.decodeByteArray(cover, 0, cover.size)
            try {
                val coverFile = File(coverPath)
                coverFile.createNewFile()
                val out = FileOutputStream(coverFile, false)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
                out.flush()
                out.close()
                hasCover = true
            } catch (e: IOException) {}
        }

        retriever.release()

        return listOf(title, artist, duration, album, hasCover)

    }
}
