// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fplayer/formats/db.dart';
import 'package:fplayer/home/home.dart';
import 'package:fplayer/login/login.dart';
import 'package:fplayer/other/appbar.dart';

import 'package:fplayer/other/wait.dart';
import 'package:fplayer/settings/settings.dart';

void main() {
  testWidgets('Check if progress indicator is shown on wait page', (WidgetTester tester) async {
    // Build wait page
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: WaitPage()
      ),
    ));
    await tester.pump(Duration(seconds: 2));
    expect(find.byType(CircularProgressIndicator), findsOneWidget);
  });

  testWidgets('Check login page', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
          body: LoginPage(null)
      ),
    ));
    // Check if login screen is loaded
    expect(find.text('Login using your Google account'), findsOneWidget);
    expect(find.text('Login'), findsOneWidget);
  });

  testWidgets('Check settings page', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
          body: SettingsPage(null)
      ),
    ));
    // Check if settings app start in wait mode
    expect(find.byType(CircularProgressIndicator), findsOneWidget);
  });

  testWidgets('Check drawer', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
          body: Bar(null)
      ),
    ));
    // Check if settings app start in wait mode
    expect(find.text('Home'), findsOneWidget);
    expect(find.text('Settings'), findsOneWidget);
  });


}
