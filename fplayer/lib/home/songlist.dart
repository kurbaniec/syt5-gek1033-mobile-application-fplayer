import 'dart:convert';
import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fplayer/formats/db.dart';
import 'package:fplayer/formats/duration.dart';

import 'package:fplayer/main.dart';

import '../home/home.dart';


class SongList extends StatelessWidget {
  MyAppState callback;
  List<Song> songs;
  bool isPlaylist;
  HomePlaylistPage page;
  Function afterOnTap;
  Function afterOnPressed;
  Function afterPlaylistUpdate;


  SongList({
    @required this.callback,
    @required this.songs,
    @required this.isPlaylist,
    this.page,
    this.afterOnTap,
    this.afterOnPressed,
    this.afterPlaylistUpdate
  });

  Future<void> _playlistDialog(
    BuildContext context, Song song
  ) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Options'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                MaterialButton(
                  color: Colors.black,
                  child: Row(
                    children: <Widget>[
                      Text('Add song to other Playlists',
                        style: TextStyle(color: Colors.white),),
                      Spacer(),
                      Icon(Icons.add, color: Colors.white,),
                    ],
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _songOptionsDialog(context, song);
                  },
                ),
                MaterialButton(
                  color: Colors.black,
                  child: Row(
                    children: <Widget>[
                      Text('Remove song from this Playlist',
                        style: TextStyle(color: Colors.white),),
                      Spacer(),
                      Icon(Icons.delete, color: Colors.white,),
                    ],
                  ),
                  onPressed: () async {
                    await page.removeSongFromPlaylist(song);
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _songOptionsDialog(
    BuildContext context, Song song
  ) async {
    List<Playlist> playlists = await callback.db.getPlaylists();
    playlists.sort((Playlist a, Playlist b) => a.compareTo(b));

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Add song to Playlists'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Playlists:'),
                SizedBox(height: 20,),
                Container(
                  width: 100,
                  height: 150,
                  child: ListView.builder(
                    itemCount: playlists.length,
                    itemBuilder: (BuildContext context, int i) {
                      return MaterialButton(
                        hoverColor: Colors.pinkAccent,
                        focusColor: Colors.pinkAccent,
                        color: Colors.pink,
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text(playlists[i].title,
                            style: TextStyle(color: Colors.black),),
                        ),
                        onPressed: () async {
                          playlists[i].addSong(song);
                          callback.db.updatePlaylist(playlists[i]);
                          await callback.change.uploadConfig();
                          if (afterPlaylistUpdate != null) afterPlaylistUpdate();
                        },
                      );
                    }
                  ),
                ),
                MaterialButton(
                  color: Colors.black,
                  child: Row(
                    children: <Widget>[
                      Text('Add new Playlist', style: TextStyle(color: Colors.white),),
                      Spacer(),
                      Icon(Icons.add, color: Colors.white,),
                    ],
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _addPlaylist(context, song);
                  },
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Done'),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _addPlaylist(
    BuildContext context, Song song,
  ) async {
    TextEditingController textController = TextEditingController();
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Create new Playlist'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextField(
                  controller: textController,
                  autofocus: true,
                  decoration: InputDecoration(
                    labelText: 'Title'
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
                _songOptionsDialog(context, song);
              },
            ),
            FlatButton(
              child: Text('Accept'),
              onPressed: () async {
                String text = textController.text;
                if (text == '') text = 'Playlist';
                callback.db.addPlaylist(text);
                await callback.change.uploadConfig();
                Navigator.of(context).pop();
                if (afterPlaylistUpdate != null) afterPlaylistUpdate();
                _songOptionsDialog(context, song);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: songs.length,
        itemBuilder: (BuildContext context, int i) {
          Widget image = songs[i].image;
          return Container(
              height: 65,
              color: Colors.black,
              child: InkWell(
                child: Row(
                  children: <Widget>[
                    SizedBox(
                      width: 65,
                      height: 65,
                      child: image,
                    ),
                    SizedBox(width: 5,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Spacer(),
                        Text(
                          songs[i].title,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 5,),
                        Text(
                          '${songs[i].artist} ' +
                              '${utf8.decode([0xE2, 0x80 , 0xA2])} ' +
                              DurationUtilities.printTime(songs[i].duration),
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.white),
                        ),
                        Spacer()
                      ],
                    ),
                    Spacer(),
                    IconButton(
                      color: Colors.white,
                      icon: Icon(Icons.more_vert),
                      onPressed: () {
                        if (isPlaylist) {
                          _playlistDialog(context, songs[i]);
                        } else {
                          _songOptionsDialog(context, songs[i]);
                        }
                        if (afterOnPressed != null) afterOnPressed();
                      },
                    )
                  ],
                ),
                onTap: () {
                  //playStop(_songs[i]);
                  callback.audio.playSong(songs[i], songs);
                  if (afterOnTap != null) afterOnTap();
                },
              )
          );
        }
    );
  }
}