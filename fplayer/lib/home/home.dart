import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fplayer/formats/db.dart';
import 'package:fplayer/model/audio.dart';
import 'package:fplayer/other/appbar.dart';

import 'package:fplayer/formats/network.dart';
import 'package:fplayer/main.dart';
import '../home/songlist.dart';

class Home extends StatefulWidget {
  // Parameters
  MyAppState callback;
  // Constructor
  Home(this.callback);
  
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Song> _songs;
  List<Album> _albums;
  List<Playlist> _playlists;
  AudioPlayerBar _player;

  MyAppState get appState => widget.callback;

  @override
  void initState() {
    super.initState();
    _songs = List();
    _albums = List();
    _playlists = List();
    _player = widget.callback.audio.getPlayerWidget();
    widget.callback.networkCallback = networkCallback;
    widget.callback.change.afterChangeFunc = setup;
    setup();
  }

  @override
  void dispose() {
    super.dispose();
    widget.callback.networkCallback = null;
    widget.callback.change.afterChangeFunc = null;
  }

  Future<void> setup() async {
    getSongs();
    getAlbums();
    getPlaylists();
  }

  Future<void> getSongs() async {
    List<Song> tmp = await widget.callback.db.getSongs();
    _songs.clear();
    _songs.addAll(tmp);
    setState(() {
      _songs.sort((Song a, Song b) => a.compareTo(b));
    });
  }

  Future<void> getAlbums() async {
    List<Album> tmp = await widget.callback.db.getAlbums();
    _albums.clear();
    _albums.addAll(tmp);
    setState(() {
      _albums.sort((Album a, Album b) => a.compareTo(b));
    });
  }

  Future<void> getPlaylists() async {
    List<Playlist> tmp = await widget.callback.db.getPlaylists();
    _playlists.clear();
    _playlists.addAll(tmp);
    setState(() {
      _playlists.sort((Playlist a, Playlist b) => a.compareTo(b));
    });
  }

  void networkCallback(Network network) {
    if (network == Network.online) {
      print("NETWORK CHANGE2: ONLINE");
    } else print("NETWORK CHANGE2: OFFLINE");
    // TODO disable streams when offline
  }

  Widget _buildAlbums(BuildContext context) {
    List<Widget> albumWidgets = List();
    for (Album album in _albums) {
      albumWidgets.add(
        Container(
          padding: const EdgeInsets.fromLTRB(8, 8, 8, 14),
          child: GestureDetector(
            child: Container(
              color: Colors.pink,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  album.image,
                  Padding(
                    padding: EdgeInsets.fromLTRB(2, 2, 2, 0),
                    child: Text(album.title,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.white, fontSize: 16)),
                  )
                ],
              ),
            ),
            onTap: () {
              Navigator.push(context,
                MaterialPageRoute(builder: (context)
                => HomePlaylistPage(
                    widget.callback.audio, this, false, album: album
                  )
                ));// Needed to update player
            },
          )
        )
      );
    }

    return GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 1/1.15,
      children: albumWidgets
    );
  }

  Widget _buildPlaylists(BuildContext context) {

    List<Widget> playerWidgets = List();
    for (Playlist playlist in _playlists) {
      List<Widget> covers = List();
      for (int i = 0; i < 4; i++) {
        Widget cover;
        if (playlist.songs.length >= i + 1) {
          cover = playlist.songs[i].image;
        }
        else cover = FormatUtils.generateBox("fff" * i);
        EdgeInsets padding;
        switch (i) {
          case 0:
            padding = EdgeInsets.fromLTRB(0, 0, 2, 2);
            break;
          case 1:
            padding = EdgeInsets.fromLTRB(2, 0, 0, 2);
            break;
          case 2:
            padding = EdgeInsets.fromLTRB(0, 2, 2, 0);
            break;
          case 3:
            padding = EdgeInsets.fromLTRB(2, 2, 0, 0);
            break;
        }
        covers.add(Padding(
          padding: padding,
          child: cover,
        ));
      }

      Widget image = GridView.count(
        crossAxisCount: 2,
        children: covers,
      );

      playerWidgets.add(
          Container(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 14),
              child: GestureDetector(
                child: Container(
                  color: Colors.pink,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: image,
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(2, 2, 2, 0),
                        child: Text(playlist.title,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Colors.white, fontSize: 16)),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context)
                      => HomePlaylistPage(
                          widget.callback.audio, this, true, playlist: playlist
                      )
                      ));// Needed to update player
                },
              )
          )
      );
    }

    return GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 1/1.15,
      children: playerWidgets
    );
  }

  void updateAppBar() {
    widget.callback.audio.setWidget(_player);
    _player.refresh();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () async => false,
      child: DefaultTabController(
          length: 3,
          child: Scaffold(
              appBar: AppBar(
                title: Text('fplayer'),
                bottom: TabBar(
                  tabs: <Widget>[
                    Tab(text: 'All songs',),
                    Tab(text: 'Playlists',),
                    Tab(text: 'Albums'),
                  ],
                ),
              ),
              drawer: Bar(widget.callback),
              bottomNavigationBar: _player,
              body: TabBarView(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.black
                    ),
                    child: Center(
                        child: Stack(
                          fit: StackFit.loose,
                          children: <Widget>[
                            SongList(
                              callback: widget.callback,
                              songs: _songs,
                              isPlaylist: false,
                              afterOnTap: () {
                                setState(() {});
                              },
                              afterOnPressed: () {
                                setState(() {});
                              },
                              afterPlaylistUpdate: () async {
                                await getPlaylists();
                                setState(() {});
                              }
                            )
                          ],
                        )
                    ),
                  ),
                  Container(
                    color: Colors.black,
                    child: _buildPlaylists(context),
                  ),
                  Container(
                    color: Colors.black,
                    child: _buildAlbums(context),
                  ),
                ],
              )
          )
      )
    );
  }

}

class HomePlaylistPage extends StatefulWidget {
  String _title;
  List<Song> _songs;
  AudioManager _audio;
  _HomeState _callback;
  bool _isPlaylist;
  Playlist _playlist;
  Album _album;
  _HomePlaylistState _state;

  HomePlaylistPage(this._audio, this._callback, this._isPlaylist,
      {Playlist playlist, Album album}
  ) {
    if (_isPlaylist) {
      _title = playlist.title;
      _songs = playlist.songs;
      _playlist = playlist;
    } else {
      _title = album.title;
      _songs = album.songs;
      _album = album;
    }
  }

  Future<void> changePlaylistTitle(String title) async {
    _playlist.title = title;
    await _callback.appState.db.updatePlaylist(_playlist);
    await _callback.appState.change.uploadConfig();
    _state.refresh();
  }

  Future<void> removeSongFromPlaylist(Song song) async {
    _playlist.removeSong(song);
    await _callback.appState.db.updatePlaylist(_playlist);
    await _callback.appState.change.uploadConfig();
    _callback.appState.audio.checkIfEmpty();
    _state.songs = _playlist.songs;
  }

  @override
  _HomePlaylistState createState() {
    _state = _HomePlaylistState();
    return _state;
  }
}

class _HomePlaylistState extends State<HomePlaylistPage> {
  List<Song> _songs;
  String _title;

  @override
  void initState() {
    super.initState();
    _songs = widget._songs;
    _songs.sort((Song a, Song b) => a.compareTo(b));
    _title = widget._title;
  }

  set title(String title) {
    setState(() {
      _title = title;
    });
  }

  set songs(List<Song> songs) {
    setState(() {
      _songs = songs;
      _songs.sort((Song a, Song b) => a.compareTo(b));
    });
  }

  void refresh() {
    setState(() {});
  }

  Future<void> _renamePlaylist(BuildContext context, Playlist playlist) async {
    TextEditingController textController = TextEditingController();
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Rename Playlist'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextField(
                  controller: textController,
                  autofocus: true,
                  decoration: InputDecoration(
                      labelText: 'Title',
                      hintText: playlist.title
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Accept'),
              onPressed: () async {
                String text = textController.text;
                if (text == '') text = 'Playlist';
                playlist.title = text;
                widget._callback.appState.db.updatePlaylist(playlist);
                await widget._callback.appState.change.uploadConfig();
                title = playlist.title;
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _playlistDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Options'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                MaterialButton(
                  color: Colors.black,
                  child: Row(
                    children: <Widget>[
                      Text('Rename Playlist',
                        style: TextStyle(color: Colors.white),),
                      Spacer(),
                      Icon(Icons.edit, color: Colors.white,),
                    ],
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _renamePlaylist(context, widget._playlist);
                  },
                ),
                MaterialButton(
                  color: Colors.black,
                  child: Row(
                    children: <Widget>[
                      Text('Remove this Playlist',
                        style: TextStyle(color: Colors.white),),
                      Spacer(),
                      Icon(Icons.delete, color: Colors.white,),
                    ],
                  ),
                  onPressed: () async {
                    await widget._callback.appState.db.deletePlaylist(
                      widget._playlist.id
                    );
                    await widget._callback.appState.change.uploadConfig();
                    Navigator.of(context).pop();
                    widget._callback.updateAppBar();
                    widget._callback.setup();
                    await widget._callback.appState.audio.stop();
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget appBar;
    if (widget._isPlaylist) {
      appBar = AppBar(
        iconTheme: IconThemeData(
            color: Colors.white
        ),
        title: Text(_title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: () {
             _playlistDialog(context);
            },
          )
        ],
      );
    } else {
      appBar = AppBar(
        title: Text(_title),
      );
    }

    return WillPopScope(
      onWillPop: () async {
        widget._callback.updateAppBar();
        await widget._callback.setup();
        return true;
      },
      child: Scaffold(
          appBar: appBar,
          bottomNavigationBar: AudioPlayerBar(widget._audio),
          body: Container(
              color: Colors.black,
              child: SongList(
                callback: widget._callback.appState,
                songs: _songs,
                isPlaylist: widget._isPlaylist,
                page: widget,
              )
          ),
      ),
    );
  }
}

