import 'dart:async';
import 'dart:io' as io;

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:fplayer/formats/audio.dart';
import 'package:fplayer/formats/db.dart';
import 'package:fplayer/formats/drive.dart';
import 'package:fplayer/formats/mode.dart';
import 'package:fplayer/formats/network.dart';
import 'package:fplayer/login/login.dart';
import 'package:fplayer/model/audio.dart';
import 'package:fplayer/model/auth.dart';
import 'package:fplayer/model/change.dart';
import 'package:fplayer/model/db.dart';
import 'package:fplayer/other/wait.dart';
import 'package:fplayer/settings/settings.dart';
import 'package:path_provider/path_provider.dart';

import 'model/drive.dart';
import 'home/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  AuthManager _auth;
  DriveManager _drive;
  DBManager _db;
  AudioManager _audio;
  ChangeManager _change;
  bool _initialized = false;
  Mode _mode = Mode.wait;
  Network _network;
  StreamSubscription _subscription;
  String _configId;
  DriveFolder _folder;
  io.Directory _appDir;
  io.Directory _musicDir;
  io.Directory _coverDir;
  bool _configInitialized = false;

  Function(Network) _networkCallback;
  final GlobalKey<NavigatorState> _navigator = GlobalKey<NavigatorState>();

  set folder (DriveFolder folder) => _folder = folder;
  set networkCallback(Function(Network) func) => _networkCallback = func;
  AuthManager get auth => _auth;
  DriveManager get drive => _drive;
  DBManager get db => _db;
  AudioManager get audio => _audio;
  ChangeManager get change => _change;
  bool get initialized => _initialized;
  Network get network => _network;
  bool get ready => (_initialized && network == Network.online);
  String get displayName => _auth.displayName;
  DriveFolder get folder => _folder;
  String get configId => _configId;
  String get musicDir => _musicDir.path;
  String get coverDir => _coverDir.path;

  @override
  void initState() {
    super.initState();
    syncSetup();
    asyncSetup();
  }

  @override
  void dispose() {
    super.dispose();
    _subscription.cancel();
  }

  void syncSetup() {
    bool initialized = false;
    // Create auth manager
    final auth = AuthManager(this);
    // Create drive manager
    final drive = DriveManager(this);
    // Create database manager
    final db = DBManager(this);
    // Create audio manager
    final audio = AudioManager(this);
    // Create change manager
    final change = ChangeManager(this);
    // Save important paths
    setState(() {
      _auth = auth;
      _drive = drive;
      _db = db;
      _audio = audio;
      _change = change;
      _initialized = initialized;
    });
  }

  void reset() {
    _initialized = false;
    _configInitialized = false;
    _networkCallback = null;
    _change.stop();
    _auth = AuthManager(this);
    _drive = DriveManager(this);
    _db = DBManager(this);
    _audio = AudioManager(this);
    _change = ChangeManager(this);
  }

  Future<void> asyncSetup({Network network}) async {
    // String devicePath = await DBManager.configPathDeviceAsync;
    // String remotePath = await DBManager.configPathRemoteAsync;
    // await io.File(devicePath).delete();
    // await io.File(remotePath).delete();

    Mode mode = _mode;
    // Check if user is already signed in
    if (mode == Mode.wait) {
      // Save important directories
      _appDir = await getApplicationDocumentsDirectory();
      _coverDir = io.Directory('${_appDir.path}/covers');
      _musicDir = io.Directory('${_appDir.path}/music');
      // Check login status
      mode = await _auth.isSignedIn() ? Mode.home : Mode.login;
    }
    if (mode == Mode.home) {
      // Check if a database exists
      // If not, then the user needs to redo login process
      String devicePath = await DBManager.configPathDeviceAsync;
      String remotePath = await DBManager.configPathRemoteAsync;
      if (!await io.File(devicePath).exists() ||
          !await io.File(remotePath).exists()) {
        mode = Mode.login;
      }
    }
    // Check network status
    if (network == null) network = await _checkNetwork();
    // User cannot login if offline
    if (mode == Mode.login && network == Network.offline)
      mode = Mode.loginOffline;
    // If user went online return to login page
    if (mode == Mode.loginOffline && network == Network.online)
      mode = Mode.login;
    // If the user is signed in and online
    // Initialize Google services
    if ((mode == Mode.home || mode == Mode.loginSelection)
        && network == Network.online )
    {
      if (!_auth.initialized)   await _auth.init();
      if (!_drive.initialized)  await _drive.init();
      if (!_db.initialized)     await _db.init();
      if (!_audio.initialized)  await _audio.init();
      if (_auth.initialized && _drive.initialized && _db.initialized) {
        _initialized = true;
      }
    } else if (mode == Mode.home && network == Network.offline) {
      if (!_auth.initialized)   await _auth.init();
      if (!_db.initialized)     await _db.init();
      if (!_audio.initialized)  await _audio.init();
    }
    // Check if user is in login process
    if (mode == Mode.loginSelection) {
      // If user logins on new devices skip configuration and use existing one
      if (await _drive.isConfigInitialized(_db.configPath)) {
        await _db.reset();
        _db.setFolder(_folder);
        _mode = Mode.loginSetup;
        _configInitialized = true;
        asyncSetup();
      }
    }
    // Load config
    if (mode == Mode.home && _initialized && await _db.driveFolderIsSet()) {
      _folder = await _db.getFolder();
      try {
        _configId = (await _db.getDriveConfig()).configId;
        // Start synchronization process
        change.start();
      } catch(_) {
        await auth.logoutReset();
        mode = Mode.login;
      }
    }
    // Set mode
    _mode = mode;
    // Set network
    _network = network;
    // Add network listenener
    if (_subscription == null)
      _subscription = Connectivity().onConnectivityChanged.listen(_networkChange);
    // Route to right page
    goTo();
  }

  void goTo() {
    switch (_mode) {
      case Mode.home:
        _navigator.currentState.pushReplacementNamed('/home');
        break;
      case Mode.login:
        _navigator.currentState.pushReplacementNamed('/login');
        break;
      case Mode.loginSelection:
        _navigator.currentState.pushNamed('/loginSelection');
        break;
      case Mode.loginConfirmation:
        _navigator.currentState.pushNamed('/loginConfirmation');
        break;
      case Mode.loginOffline:
        _navigator.currentState.pushReplacementNamed('/loginOffline');
        break;
      case Mode.loginSetup:
        _navigator.currentState.pushReplacementNamed('/loginSetup');
        loginSetup();
        break;
      case Mode.wait:
        break;
    }
  }

  /// Is called when the authentication status changes.
  /// A change is when a user logs out for example.
  Future<void> authChange(isLoggedIn) async {
    if (isLoggedIn) {
      _mode = Mode.loginSelection;
    } else _mode = Mode.login;
    asyncSetup();
  }

  Future<void> loginSelection(DriveFolder folder) async {
    _folder = folder;
    _db.setFolder(_folder);
    _mode = Mode.loginConfirmation;
    asyncSetup();
  }

  void loginConfirmation() async {
    _mode = Mode.loginSetup;
    asyncSetup();
  }

  void loginSetup() async {
    // Create app relevant directories
    if (await _coverDir.exists()) _coverDir.deleteSync(recursive: true);
    _coverDir.createSync(recursive: true);

    if (await _musicDir.exists()) {
      _musicDir.deleteSync(recursive: true);
    }
    _musicDir.createSync(recursive: true);
    // Get drive token
    String token = await _drive.getToken();
    // Scan and get all songs from drive
    List<SongMetaData> songs
      = await _drive.getSongs(_folder.id, true);
    // Convert SongMetaData to Song type and save in database
    for (SongMetaData songRaw in songs) {
      // Convert type
      Song song = Song(
        id: songRaw.id,
        title: songRaw.title,
        artist: songRaw.artist,
        duration: Duration(milliseconds: int.parse(songRaw.duration)),
        cover: songRaw.hasCover,
        coverPath: songRaw.coverPath,
        offline: songRaw.offline,
        offlinePath: songRaw.offlinePath
      );
      // Add Album info
      if (songRaw.album != null) {
        Album album = Album(title: songRaw.album);
        int albumId = await _db.addAlbum(album);
        song.albumId = albumId;
      }
      // Save it
      await _db.addSong(song);
    }
    // Add default synchronization config
    var syncConfig = SynchronizationConfig(enabled: true, duration: "60");
    await _db.addSynchronizationConfig(syncConfig);
    // Upload configuration
    if (!_configInitialized)
      _configId = await _drive.uploadFile(folder.id, io.File(_db.configPath));
    // Save drive token and config id
    await _db.setConfig(token, _configId, _folder);
    _mode = Mode.home;
    asyncSetup();
  }

  void loginDisprove() async {
    await _auth.logout();
    _folder = null;
    _navigator.currentState.pushNamed('/login');
  }

  Future<Network> _checkNetwork() async {
    Network network;
    try {
      final result = await io.InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        network = Network.online;
      } else network = Network.offline;
    } on io.SocketException catch(_) {
      network = Network.offline;
    }
    if (network == Network.online) {
      print("NETWORK CHANGE: ONLINE");
    } else print("NETWORK CHANGE: OFFLINE");
    return network;
  }

  Future<void> _networkChange(ConnectivityResult result) async {
    // Check network status
    final network = await _checkNetwork();
    // If app is not on home page, continue login process
    if (_mode != Mode.home) await asyncSetup(network: network);
    // Else, do home page check
    else {
      // If Google service are not initialized yet, initialize them
      if (!initialized) await asyncSetup(network: network);
      // Manage change manager
      if (initialized) {
        if (network == Network.online) {
          _change.start();
        }
        else _change.stop();
      }
      // Else, just set network
      else {
        setState(() {
          _network = network;
        });
      }
      // Call network-callback
      if (_networkCallback != null) {
        _networkCallback(_network);
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    Widget page = new WaitPage();

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.pink,
      ),
      home: page,
      navigatorKey: _navigator,
      routes: <String, WidgetBuilder>{
        '/wait': (context) => new WaitPage(),
        '/home': (context) => new Home(this),
        '/settings': (context) => SettingsPage(this),
        '/login': (context) => new LoginPage(this),
        '/loginSelection': (context) => new LoginSelectionPage(this),
        '/loginConfirmation': (context) => new LoginConfirmationPage(this),
        '/loginSetup': (context) => new LoginSetupPage(),
        '/loginOffline': (context) => new LoginOfflinePage(),
      },
    );
  }

}


