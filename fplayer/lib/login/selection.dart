
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fplayer/formats/drive.dart';

class FolderSelection extends StatefulWidget {

  final DriveFolder _folderStructure;
  final Function(DriveFolder) _onComplete;

  FolderSelection({
    @required DriveFolder driveFolder,
    @required Function(DriveFolder) onComplete
  }) :
    _folderStructure = driveFolder,
    _onComplete = onComplete;

  @override
  _FolderSelectionState createState() => _FolderSelectionState();

}

class _FolderSelectionState extends State<FolderSelection> {
  DriveFolder _display;
  String selection = 'items';

  @override
  void initState() {
    super.initState();
    setState(() {
      _display = widget._folderStructure;
    });
  }

  void displayChildren(int index) {
    setState(() {
      _display = _display.children[index];
    });
  }

  void select(int index) {
    DriveFolder select = _display.children[index];
    widget._onComplete(select);
  }

  void stepBack() {
    setState(() {
      _display = _display.parent;
    });
  }


  Widget buildBackButton() {
    return Container(
      alignment: Alignment.centerLeft,
      child: Card(
        child: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => stepBack(),
        ),
      ),
    );
  }

  List<Widget> buildButtons(bool folderHasChildren, int index) {
    if (folderHasChildren) {
      return [
        IconButton(
          icon: Icon(Icons.arrow_downward),
          onPressed: () => this.displayChildren(index),
        ),
        IconButton(
          icon: Icon(Icons.check),
          onPressed: () => this.select(index),
        ),
      ];
    } else {
      return [
        IconButton(
          icon: Icon(Icons.check),
          onPressed: () => this.select(index),
        ),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> allElements = [];

    if (_display.parent != null) {
      allElements.add(buildBackButton());
    }

    allElements.add(ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: _display.children.length,
      itemBuilder: (context, index) {
        final folderHasChildren
          = _display.children[index].children.length != 0;
        final buttonWidgets = buildButtons(folderHasChildren, index);
        final selectorWidgets = <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: Text(
                _display.children[index].title,
              )),
          ),
        ];
        selectorWidgets.addAll(buttonWidgets);
        return Card(
          child: Row(
            children: selectorWidgets
          )
        );
      },
    ));

    return Column(
      children: allElements,
    );
  }

}