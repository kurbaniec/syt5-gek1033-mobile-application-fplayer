import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fplayer/main.dart';

import 'package:fplayer/formats/drive.dart';
import 'selection.dart';

class LoginPage extends StatefulWidget {
  MyAppState callback;
  LoginPage(this.callback, {Key key, }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState(this.callback);
}

class _LoginPageState extends State<LoginPage> {
  // Parameters
  MyAppState callback;
  // Constructor
  _LoginPageState(this.callback);
  Widget logo;


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Container(
          color: Colors.pink,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(),
                SizedBox(
                  height: 150,
                  width: 150,
                  child: Image(image: AssetImage('assets/fplayer-logo.png'),),
                ),
                SizedBox(height: 50,),
                Text(
                  'Login using your Google account',
                  style: TextStyle(fontSize: 16, color: Colors.white),
                ),
                SizedBox(height: 20.0),
                MaterialButton(
                  color: Colors.black,
                  child: Text('Login', style: TextStyle(color: Colors.white),),
                  onPressed: _handleSignIn,
                ),
                Spacer(),
              ],
            ),
          ),
        ),
      )
    );
  }

  Future<void> _handleSignIn() async {
    this.callback.auth.login();
  }
}

class LoginSelectionPage extends StatefulWidget {
  MyAppState callback;
  LoginSelectionPage(this.callback, {Key key}) : super(key: key);

  @override
  _LoginSelectionPageState createState() => _LoginSelectionPageState();
}

class _LoginSelectionPageState extends State<LoginSelectionPage> {
  DriveFolder _fileList;
  Widget _selector;

  @override
  void initState() {
    super.initState();
    setup();
    setState(() {
      _selector = CircularProgressIndicator(
        backgroundColor: Colors.pink,
        strokeWidth: 5,);
    });
  }

  Future<void> setup() async {
    var fileList = await buildFileList();
    var selector = await buildSelector(fileList);
    if (mounted) {
      setState(() {
        _fileList = fileList;
        _selector = selector;
      });
    }
  }

  void selectionDone(DriveFolder folder) {
    widget.callback.loginSelection(folder);
  }

  Future<DriveFolder> buildFileList() async {
    if (widget.callback.drive.initialized) {
      var folderStructure = await widget.callback.drive.getFolders();
      print(folderStructure);
      return folderStructure;
    }
    return null;
  }

  Future<Widget> buildSelector(fileList) async {
    if (fileList == null) {
      setup();
      return CircularProgressIndicator(
        backgroundColor: Colors.pink,
        strokeWidth: 5,);
    } else return FolderSelection(
      driveFolder: fileList,
      onComplete: (DriveFolder folder) => selectionDone(folder)
    );
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    if (_fileList == null) {
      setup();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Select location'),
      ),
      body: Container(
        decoration: BoxDecoration(
            color: Colors.black
        ),
        child: Center(
          child: _selector,
        ),
      ),
    );
  }
}

class LoginConfirmationPage extends StatefulWidget {
  MyAppState callback;
  LoginConfirmationPage(this.callback, {Key key}) : super(key: key);

  @override
  _LoginConfirmationPageState createState() => _LoginConfirmationPageState();
}

class _LoginConfirmationPageState extends State<LoginConfirmationPage> {
  DriveFolder _fileList;

  @override
  void initState() {
    super.initState();
    setup();
  }

  Future<void> setup() async {

  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Confirm settings'),
      ),
      body: Container(
        decoration: BoxDecoration(
            color: Colors.black
        ),
        child: Center(
          child: Container (
            height: 190,
            width: 190,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: 190,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          " Settings correct?",
                          style: TextStyle(
                              fontSize: 23
                        )),
                        Text(
                            " User: " + widget.callback.displayName + "\n" +
                            " Folder: " + widget.callback.folder.title + "\n",
                        style: TextStyle(
                            fontSize: 18
                        )),
                      ],
                    )
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Card(
                      child: IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () => widget.callback.loginDisprove(),
                      ),
                    ),
                    Card(
                      child: IconButton(
                        icon: Icon(Icons.check),
                        onPressed: () => widget.callback.loginConfirmation(),
                      ),
                    ),
                  ]
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class LoginSetupPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            color: Colors.black
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "Downloading music libary...\nThis can take a while...",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              SizedBox(height: 35),
              SizedBox(
                width: 150,
                height: 150,
                child: CircularProgressIndicator(),
              ),
            ],
          ),
        ),
      ),
    );
  }

}

class LoginOfflinePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            color: Colors.black
        ),
        child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                    "Waiting for network...",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                SizedBox(height: 35),
                SizedBox(
                  width: 150,
                  height: 150,
                  child: CircularProgressIndicator(),
                ),
              ],
            ),
        ),
      ),
    );
  }

}