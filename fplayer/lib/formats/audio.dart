import 'package:flutter/cupertino.dart';

class SongMetaData {
  String title;
  String artist;
  String duration;
  String album;
  bool hasCover;

  String id;
  bool offline;
  String offlinePath;
  String coverPath;

  SongMetaData(this.title, this.artist, this.duration, this.album, this.hasCover);

  SongMetaData.complete(SongMetaData raw, {
    @required this.id,
    @required this.offline,
    this.offlinePath,
    this.coverPath,
  });
}