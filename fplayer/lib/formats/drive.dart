
import 'package:dataclass/dataclass.dart';

@dataClass
class DriveFolder {
  final String id;
  final String title;
  final DriveFolder parent;
  final List<DriveFolder> children;

  DriveFolder(this.title, this.id, this.parent, this.children);

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
    };
  }
}