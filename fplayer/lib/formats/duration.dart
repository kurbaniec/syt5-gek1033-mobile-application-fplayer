class DurationUtilities {
  static String printTime(Duration d) {
    // Source: https://stackoverflow.com/a/54775297/12347616
    String twoDigits(int n) => n >= 10 ? "$n" : "0$n";
    String hours = twoDigits(d.inHours);
    String minutes = twoDigits(d.inMinutes.remainder(60));
    String seconds = twoDigits((d.inSeconds.remainder(60)));
    if (hours != "00") {
      return "$hours:$minutes:$seconds";
    }
    return "$minutes:$seconds";
  }
}