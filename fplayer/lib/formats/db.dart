
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DriveConfig {
  String token;
  String configId;
  String folderId;

  DriveConfig(this.token, this.configId, this.folderId);

  Map<String, dynamic> toMap() {
    return {
      'token': token,
      'configid': configId,
      'folderid': folderId,
    };
  }
}

class SynchronizationConfig {
  int id;
  bool enabled;
  String _duration;

  SynchronizationConfig({this.id, this.enabled, String duration})
      : _duration = duration;

  set duration(Duration duration) {
    _duration = "${duration.inSeconds.toString()}";
  }
  Duration get duration {
    return Duration(seconds: int.parse(_duration));
  }

  Map<String, dynamic> toMap() {
    return {
      'enabled': enabled ? 1:0,
      'duration': "'$_duration'"
    };
  }

  static parseDuration(String duration) {
    return duration.substring(1, duration.length-1);
  }
}

class Playlist implements Comparable<Playlist> {
  int id;
  String title;
  String songIds;
  List<Song> songs = List();

  Playlist({this.id, this.title, this.songIds});

  Playlist.create({@required this.title}) {
    this.songIds = "||";
  }

  addSong(Song song) {
    if (!songIds.contains("||${song.id}||"))
      songIds += "${song.id}||";
  }

  removeSong(Song song) {
    if (songs.contains(song)) songs.remove(song);
    songIds = songIds.replaceAll("||${song.id}||", "||");
  }

  List<String> getSongIds() {
    if (songIds == "||") return List();
    String ids = songIds.substring(2, songIds.length-2);
    return ids.split("||");
  }

  String songIdsQuery() {
    String query = "";
    int length = getSongIds().length;
    for (int i = 0; i < length; i++) {
      if (i != length-1) {
        query += "id = ? OR ";
      } else {
        query += "id = ?";
      }
    }
    return query;
  }

  bool combine(Playlist other) {
    bool changes = false;
    if(this.title != other.title) {
      this.title = other.title;
      changes = true;
    }
    List<String> thisIds = this.getSongIds();
    List<String> otherIds = other.getSongIds();
    for (String id in otherIds) {
      if (!thisIds.contains(id)) {
        songIds += "$id||";
        changes = true;
      }
    }
    return changes;
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'songids': songIds
    };
  }

  @override
  int compareTo(Playlist other) {
    return this.title.compareTo(other.title);
  }

  @override
  bool operator == (Object other) {
    if (other is Playlist && this.hashCode == other.hashCode) {
      if (this.id == other.id && this.title == this.title &&
          this.songIds == other.songIds) {
        return true;
      }
    }
    return false;
  }

  @override
  int get hashCode => id + title.length + songIds.length;
}

class Album implements Comparable<Album> {
  int id;
  String title;
  List<Song> songs;

  Album({this.title, this.id, this.songs});

  Map<String, dynamic> toMap() {
    return {
      'title': title
    };
  }

  Widget get image {
    if (songs != null && songs.length > 0) {
      return Image.file(File(songs[0].coverPath));
    }
    return FormatUtils.generateBox(title);
  }

  @override
  int compareTo(Album other) {
    return this.title.compareTo(other.title);
  }
}

class Song implements Comparable<Song> {
  final String id;
  String title;
  String artist;
  Duration duration;
  int albumId;
  String playlistIds;
  bool cover;
  String coverPath;
  bool offline;
  String offlinePath;

  set playlists(List<String> playlistIds) {
    this.playlistIds = playlistIds.join("|");
  }

  Widget get image {
    if (cover) {
      return Image.file(File(coverPath));
    }
    return FormatUtils.generateBox(title);
  }

  List<String> get playlists {
    this.playlistIds.split("|");
  }

  Song({this.id, this.title, this.artist, this.duration, this.offline, this.cover,
      this.albumId, this.playlistIds, this.coverPath, this.offlinePath});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'artist': artist,
      'duration': '${duration.inMilliseconds}',
      'album': albumId,
      'playlists': playlistIds,
      'cover': cover ? 1:0,
      'coverpath': coverPath,
      'offline': offline ? 1:0,
      'offlinepath': offlinePath,
    };
  }

  @override
  int compareTo(Song other) {
    return this.title.compareTo(other.title);
  }

}

class FormatUtils {
  static Widget generateBox(String title) {
    int colorIndex = title.length * 25;
    int check = colorIndex % 100;
    if (check != 0) {
      colorIndex = (100 - check % 100) + colorIndex;
    }
    if (colorIndex == 0) colorIndex = 100;
    if (colorIndex >= 900) colorIndex = 900;
    Color c = Colors.pink[colorIndex];
    return Container(
        color: c,
        child: SizedBox.expand()
    );
  }
}


