import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WaitPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            color: Colors.black
          ),
          child: Center(
            child: SizedBox(
              width: 150,
              height: 150,
              child: CircularProgressIndicator(),
            )
          ),
        ),
    );
  }

}