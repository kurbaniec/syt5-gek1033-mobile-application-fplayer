import 'package:flutter/material.dart';
import 'package:fplayer/main.dart';

class Bar extends StatelessWidget {
  MyAppState callback;

  Bar(this.callback);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('fplayer', style: TextStyle(color: Colors.white),),
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.pink,
              ),
            ),
            ListTile(
                title: Text('Home'),
                onTap: () => Navigator.of(context).pushReplacementNamed('/home')
              // Update the state of the app.
              // ...

            ),
            ListTile(
              title: Text('Settings'),
              onTap: () => Navigator.of(context).pushReplacementNamed('/settings')
                // Update the state of the app.
                // ...

            ),
          ]
      ),
    );
  }
}