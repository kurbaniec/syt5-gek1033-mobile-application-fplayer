import 'dart:async';
import 'dart:io';

import 'package:fplayer/formats/audio.dart';
import 'package:fplayer/formats/db.dart';
import 'package:fplayer/formats/network.dart';
import 'package:fplayer/main.dart';
import 'package:fplayer/model/db.dart';
import 'package:googleapis/drive/v3.dart' as drive;
import 'package:path/path.dart' as path;

class ChangeManager {
  MyAppState callback;
  Timer _routineTimer;
  bool _routineWorking = false;
  Function afterChangeFunc;

  ChangeManager(this.callback);

  Future<void> start() async {
    if (callback.db.initialized) {
      SynchronizationConfig config = await callback.db.getSynchronizationConfig();
      if (_routineTimer != null) stop();
      _routineTimer = Timer.periodic(config.duration, routine);
      //_routineTimer = Timer.periodic(Duration(seconds: 15), routine);
    }
  }

  void stop() {
    if (_routineTimer != null) {
      _routineTimer.cancel();
      _routineWorking = false;
    }
  }

  Future<void> uploadConfig({SynchronizationConfig sync, bool enabled=false}) async {
    if (sync == null) sync = await callback.db.getSynchronizationConfig();
    if (sync.enabled || enabled) {
      await callback.drive.updateFile(
        callback.configId, callback.folder.id, File(callback.db.configPath)
      );
    }
  }

  /// Gets the latest changes from Google Drive.
  /// This function adds, updates and removes songs.
  Future<void> routine(Timer timer) async {
    // print("ROUTINE!");
    // Check if all services are initialized and device is online
    if (callback.initialized
        && callback.network == Network.online && !_routineWorking
    ) {
      // Start change process
      _routineWorking = true;
      try {
        // Refresh authentication
        await callback.auth.refreshAuthentication();
        await callback.drive.refreshAuthentication();
        // Get drive changes token from local database
        DriveConfig config = await callback.db.getDriveConfig();
        String token = config.token;
        // Retrieve changes with token until all changes are processed
        while (token != null) {
          drive.ChangeList changes = await callback.drive.getChanges(
              token);
          for (drive.Change change in changes.changes) {
            // Check if player configuration got changed
            if (change.fileId == callback.configId) {
              // Check if it was removed or trashed
              if (change.removed == true ||
                  (change.file != null && change.file.trashed)) {
                // Upload configuration and update device config
                String configId = await callback.drive.uploadFile(
                    callback.folder.id, File(callback.db.configPath)
                );
                DriveConfig config = await callback.db.getDriveConfig();
                await callback.db.setConfig(config.token, configId, callback.folder);
              } else {
                // Remote file was changed
                // Combine local and remote database
                String tmpPath = await DBManager.configPathTemporaryAsync;
                await callback.drive.downloadFile(change.fileId, tmpPath);
                bool difference = await callback.db.mergeRemoteDatabases(tmpPath);
                // If there is a difference between the two upload the
                // combined database
                if (difference) {
                  await callback.drive.updateFile(
                    callback.configId, callback.folder.id, File(callback.db.configPath)
                  );
                }
                await File(tmpPath).delete();
              }
            }
            // Check if the changed file is part of the music library
            // used by this app
            else if (await callback.db.songExists(change.fileId) ||
                (change.file!=null && change.file.mimeType.contains('audio/') &&
                    await callback.drive.fileInFolder(
                        change.fileId, config.folderId))
            ) {
              // Check if file was completely removed from drive
              // When a file is completely removed, only the id is given
              // Deleted song through id only
              if (change.removed) {
                await callback.db.deleteSong(change.fileId);

                Directory musicDir = Directory(callback.musicDir);
                List<FileSystemEntity> files = musicDir.listSync();
                for (var file in files) {
                  if (file is File) {
                    String filename = path.basename(file.path);
                    if (filename.startsWith(change.fileId)) await file.delete();
                  }
                }
                Directory coverDir = Directory(callback.coverDir);
                files = coverDir.listSync();
                for (var file in files) {
                  if (file is File) {
                    String filename = path.basename(file.path);
                    if (filename.startsWith(change.fileId)) await file.delete();
                  }
                }
              } else {
                // If the file was not completely removed, continue here
                // First, declare file-specific variables
                drive.File file = change.file;
                String filePath
                = '${callback.musicDir}/${file.id}.${file.fileExtension}';
                String coverPath
                = '${callback.coverDir}/${file.id}.PNG';
                File songFile = File(filePath);
                File coverFile = File(coverPath);
                // Check if file was trashed
                if (change.file.trashed) {
                  // If yes, delete file from app
                  await callback.db.deleteSong(change.fileId);
                  if (await songFile.exists()) await songFile.delete();
                  if (await coverFile.exists()) await coverFile.delete();
                } else {
                  // Else, update or add new song
                  // Donwload and process song at first
                  SongMetaData songRaw
                  = await callback.drive.downloadAndProcessSong(file);
                  Song song = Song(
                      id: songRaw.id,
                      title: songRaw.title,
                      artist: songRaw.artist,
                      duration: Duration(milliseconds: int.parse(songRaw.duration)),
                      cover: songRaw.hasCover,
                      coverPath: songRaw.coverPath,
                      offline: songRaw.offline,
                      offlinePath: songRaw.offlinePath
                  );
                  // Add album info to song
                  if (songRaw.album != null) {
                    Album album = Album(title: songRaw.album);
                    int albumId = await callback.db.addAlbum(album);
                    song.albumId = albumId;
                  }
                  // Check if songs was newly added or updated
                  bool songExists = await callback.db.songExists(file.id);
                  if (songExists) {
                    // Update entry
                    await callback.db.updateSong(song);
                  } else {
                    // Add new entry
                    await callback.db.addSong(song);
                  }
                }
              }
            }
          }
          // Update drive token
          if (changes.newStartPageToken != null) {
            config.token = changes.newStartPageToken;
          }
          token = changes.nextPageToken;
        }
        // Update config
        await callback.db.updateConfig(config);
        // Perform post change function
        if (afterChangeFunc != null) afterChangeFunc();
      } catch(e) {
        print(e);
      }
      _routineWorking = false;
    }
  }

}