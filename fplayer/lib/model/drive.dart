
import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;

import 'package:flutter/foundation.dart';
import 'package:fplayer/formats/audio.dart';
import 'package:fplayer/model/audio.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:googleapis/drive/v3.dart';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import '../formats/drive.dart';
import '../main.dart';

import 'package:path/path.dart';

class DriveManager {
  bool _initialized = false;
  DriveApi _drive;
  String _authToken;
  MyAppState callback;


  DriveManager(this.callback);

  bool get initialized => _initialized;

  Future<void> init() async {
    // Get account through callback
    GoogleSignInAccount account = callback.auth.account;
    // Stop process if something went wrong
    if (account == null) return;
    // Get access token for drive access
    GoogleSignInAuthentication authentication =
        await account.authentication;
    _authToken = authentication.accessToken;
    final client = AuthClient(defaultHeaders: {
      'Authorization': 'Bearer ${authentication.accessToken}'
    });
    // Init drive api
    _drive = DriveApi(client);
    _initialized = true;
  }

  Future<void> refreshAuthentication() async {
    await init();
  }

  /// -----
  /// LOGIN
  /// -----

  Future<FileList> getRoot() async {
    var files = await _drive.files
        .list(q: '\'root\' in parents and mimeType=\'application/vnd.google-apps.folder\'');
    return files;
  }

  Future<DriveFolder> getFolders() async {
    return await _getFolders('root', 'root');
  }

  Future<DriveFolder> _getFolders(String folderId, String folderTitle) async {
    var files = await _drive.files.list(q: '\'$folderId\' in parents and mimeType=\'application/vnd.google-apps.folder\'');

    List<DriveFolder> folderChildren = new List();
    var folderStrucutre = new DriveFolder(folderTitle, folderId, null, folderChildren);

    for (var child in files.files) {
      await getMoreFolders(folderStrucutre.children, folderStrucutre, child);
    }

    return folderStrucutre;
  }

  Future<void> getMoreFolders(List folders, DriveFolder parent, File child) async {
    File file = await _drive.files.get(child.id);
    print(file.name);

    List<DriveFolder>  folderChildren = new List();
    DriveFolder folder = DriveFolder(file.name, file.id, parent, folderChildren);
    folders.add(folder);

    String query = '\''+ child.id + '\' in parents and mimeType=\'application/vnd.google-apps.folder\'';
    var children = await _drive.files.list(q: query);

    for (var children in children.files) {
      await getMoreFolders(folderChildren, folder, children);
    }
  }

  Future<bool> isConfigInitialized(String path) async {
    String query = 'name = \'.fplayer.config\' and trashed = false';
    var check = await _drive.files.list(q: query, $fields: 'files(id, name, parents)');
    if (check.files.length > 0) {
      File config = check.files[0];
      if (config == null || config.parents.length == 0) return false;
      DriveFolder folder = await _getFolder(config.parents[0]);
      Media file = await _drive.files.get(
          config.id, downloadOptions: DownloadOptions.FullMedia
      );
      io.File localFile = io.File(path);
      Stream fileStream = file.stream;
      List<List<int>> buffer = await fileStream.toList();
      List<int> data = List();
      for(List<int> part in buffer) {
        data.addAll(part);
      }
      await localFile.writeAsBytes(data);
      callback.folder = folder;
      return true;
    } else return false;
  }

  Future<void> downloadFile(String fileId, String path) async {
    Media file = await _drive.files.get(
        fileId, downloadOptions: DownloadOptions.FullMedia
    );
    io.File localFile = io.File(path);
    Stream fileStream = file.stream;
    List<List<int>> buffer = await fileStream.toList();
    List<int> data = List();
    for(List<int> part in buffer) {
      data.addAll(part);
    }
    await localFile.writeAsBytes(data);
  }

  Future<DriveFolder> _getFolder(String folderId) async {
    File parent = await _drive.files.get(folderId);
    return DriveFolder(parent.name, parent.id, null, null);
  }

  Future<String> uploadFile(String folderId, io.File file) async {
    File upload = File();
    upload.name = basename(file.path);
    upload.parents = [folderId];

    File config = await _drive.files.create(
      upload, uploadMedia: Media(file.openRead(), file.lengthSync())
    );
    return config.id;
  }

  Future<void> updateFile(String fileId, String folderId, io.File file) async {
    File upload = File();
    upload.name = basename(file.path);
    await _drive.files.update(
      upload, fileId, uploadMedia: Media(file.openRead(), file.lengthSync()),
      addParents: folderId
    );
  }


  /// --------------
  /// Initialization
  /// --------------

  Future<String> getToken() async {
    StartPageToken token = await _drive.changes.getStartPageToken();
    return token.startPageToken;
  }

  Future<List<SongMetaData>> getSongs(String folderId, bool saveSongs) async {
    DriveFolder folders = await _getFolders(folderId, null);
    List<File> files = List();
    String musicDir = callback.musicDir;
    String coverDir = callback.coverDir;
    // Get files in root folder
    String query = '\'$folderId\' in parents and '
        'mimeType contains \'audio/\'';
    String fields = 'files(id, name, fileExtension)';
    List<File> rootFiles = (await _drive.files.list(q: query, $fields: fields)).files;
    files.addAll(rootFiles);
    // Get files from children
    for (DriveFolder folder in folders.children) {
      String query = '\'${folder.id}\' in parents and '
          'mimeType contains \'audio/\'';
      List<File> folderFiles = (await _drive.files.list(q: query, $fields: fields)).files;
      files.addAll(folderFiles);
    }
    // Get meta information
    List<Future<void>> downloadQueue = List();
    for (File file in files) {
      Map<String, dynamic> params = Map();
      params['authToken'] = _authToken;
      params['file'] = file;
      params['musicDir'] = callback.musicDir;
      Future<void> download =  compute(_donwloadSong, params);
      downloadQueue.add(download);
    }
    await Future.wait(downloadQueue);
    AudioMetaData processor = AudioMetaData();
    List<SongMetaData> songs = List();
    for (File file in files) {
      String filePath = '$musicDir/${file.id}.${file.fileExtension}';
      String coverPath = '$coverDir/${file.id}.PNG';
      SongMetaData song = await processor.readTags(filePath, coverPath);
      if (!saveSongs) {
        io.File(filePath).deleteSync(recursive: true);
      }
      song.id = file.id;
      if (saveSongs) {
        song.offline = true;
        song.offlinePath = filePath;
      } else song.offline = false;
      if (song.hasCover) song.coverPath = coverPath;
      songs.add(song);
    }
    return songs;
  }

  Future<SongMetaData> downloadAndProcessSong(File file,{bool saveSong=true}) async {
    Map<String, dynamic> params = Map();
    params['authToken'] = _authToken;
    params['file'] = file;
    params['musicDir'] = callback.musicDir;
    await compute(_donwloadSong, params);
    AudioMetaData processor = AudioMetaData();
    String filePath = '${callback.musicDir}/${file.id}.${file.fileExtension}';
    String coverPath = '${callback.coverDir}/${file.id}.PNG';
    SongMetaData song = await processor.readTags(filePath, coverPath);
    if (!saveSong) {
      io.File(filePath).deleteSync(recursive: true);
    }
    song.id = file.id;
    if (saveSong) {
      song.offline = true;
      song.offlinePath = filePath;
    } else song.offline = false;
    if (song.hasCover) song.coverPath = coverPath;
    return song;
  }

  static Future<void> _donwloadSong(Map<String, dynamic> params) async {
    bool done = false;
    while (!done) {
      String authToken = params['authToken'];
      File file = params['file'];
      String musicDir = params['musicDir'];
      try {
        final client = AuthClient(defaultHeaders: {
          'Authorization': 'Bearer $authToken'
        });
        // Init drive api
        DriveApi drive = DriveApi(client);
        // Get stream for file
        Media stream = await drive.files.get(
            file.id, downloadOptions: DownloadOptions.FullMedia
        );
        // Download song
        Stream fileStream = stream.stream;
        List<List<int>> buffer = await fileStream.toList();
        List<int> data = List();
        for (List<int> part in buffer) {
          data.addAll(part);
        }
        // Save song
        String filePath = '$musicDir/${file.id}.${file.fileExtension}';
        io.File localFile = io.File(filePath);
        await localFile.writeAsBytes(data);
        done = true;
      } catch(e) {
        print(e);
      }
    }
  }
  
  /// -------
  /// Changes
  /// -------

  Future<ChangeList> getChanges(String token) async {
    return await _drive.changes.list(token, includeRemoved: true,
      $fields: 'nextPageToken,newStartPageToken,changes(fileId,removed,'
        'file(id,parents,mimeType,trashed,fileExtension))');
  }

  Future<bool> fileInFolder(String fileId, String folderId) async {
    FileList check = await _drive.files.list(q: '\'$folderId\' in parents');

    for (File file in check.files) {
      if (file.id == fileId) return true;
    }

    FileList folders = await _drive.files.list(
        q: '\'$folderId\' in parents and mimeType=\'application/vnd.google-apps.folder\'');
    for (File folder in folders.files) {
      bool check = await fileInFolder(fileId, folder.id);
      if (check) return true;
    }
    return false;
  }
  

  /// ---------
  /// Streaming
  /// ---------
  
  Future<String> getSongDownloadUrl(String fileId) async {
    File file = await _drive.files.get(fileId, $fields: 'webContentLink');
    //String url = file.webContentLink + "&access_token=" + Uri.encodeComponent(_authToken);
    //url = url.substring(0, url.length-16);
    String url = file.webContentLink;
    url = url.substring(0, url.length-16);
    //url = url + "&access_token=" + Uri.encodeComponent(_authToken);
    //final headers = { 'Authorization': 'Bearer $_authToken' };
    //Response resp = await get(url, headers: headers);
    //print(resp);
    return url;
  }

}




/// Http-client used for drive authentication.
/// Source: https://github.com/nodes-android/google-docs-viewer-flutter/blob/master/lib/other/my_client.dart
class AuthClient extends http.BaseClient {
  final Map<String, String> defaultHeaders;
  http.Client _httpClient = new http.Client();

  AuthClient({this.defaultHeaders = const {}});

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    request.headers.addAll(defaultHeaders);
    return _httpClient.send(request);
  }

  @override
  Future<Response> get(url, {Map<String, String> headers}) {
    headers.addAll(defaultHeaders);
    return _httpClient.get(url, headers: headers);
  }

  @override
  Future<Response> post(url,
      {Map<String, String> headers, body, Encoding encoding}) {
    headers.addAll(defaultHeaders);
    return _httpClient.post(url, headers: headers, encoding: encoding);
  }

  @override
  Future<Response> patch(url,
      {Map<String, String> headers, body, Encoding encoding}) {
    headers.addAll(defaultHeaders);
    return _httpClient.patch(url, headers: headers, encoding: encoding);
  }

  @override
  Future<Response> put(url,
      {Map<String, String> headers, body, Encoding encoding}) {
    headers.addAll(defaultHeaders);
    return _httpClient.put(url,
        headers: headers, body: body, encoding: encoding);
  }

  @override
  Future<Response> head(url, {Map<String, String> headers}) {
    headers.addAll(defaultHeaders);
    return _httpClient.head(url, headers: headers);
  }

  @override
  Future<Response> delete(url, {Map<String, String> headers}) {
    headers.addAll(defaultHeaders);
    return _httpClient.delete(url, headers: headers);
  }
}