import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import '../main.dart';
import '../formats/db.dart';
import '../formats/drive.dart';

class DBManager {
  MyAppState callback;
  Database _driveDB;
  Database _deviceDB;
  bool _initialized = false;
  String _configPath;

  DBManager(this.callback);

  bool get initialized => _initialized;
  String get configPath => _configPath;
  static Future<String> get configPathRemoteAsync async {
    return join(await getDatabasesPath(), '.fplayer.config');
  }
  static Future<String> get configPathDeviceAsync async {
    return join(await getDatabasesPath(), '.fplayer.device.config');
  }
  static Future<String> get configPathTemporaryAsync async {
    return join(await getDatabasesPath(), '.tmp.config');
  }

  Future<void> init() async {

    _configPath = join(await getDatabasesPath(), '.fplayer.config');
    //await deleteDatabase(_configPath);
    _driveDB = await openDatabase(
      _configPath,
      onCreate: (db, version) {
        db.execute(
          "CREATE TABLE Playlist("
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "title TEXT,"
            "songids TEXT"
          ")"
        );
      },
      version: 1
    );

    String configPath = join(await getDatabasesPath(), '.fplayer.device.config');
    //await deleteDatabase(configPath);
    _deviceDB = await openDatabase(
      configPath,
      onCreate: (db, version) {
        db.execute(
          "CREATE TABLE Album("
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "title TEXT"
          ")"
        );
        db.execute(
          "CREATE TABLE Song("
            "id TEXT PRIMARY KEY,"
            "title TEXT,"
            "artist TEXT,"
            "duration TEXT,"
            "album INTEGER,"
            "playlists TEXT,"
            "cover BOOL,"
            "coverpath TEXT,"
            "offline BOOL,"
            "offlinepath TEXT,"
            "FOREIGN KEY(album) REFERENCES Album(id)"
          ")"
        );
        db.execute(
          "CREATE TABLE DriveFolder(id TEXT PRIMARY KEY, title TEXT);"
        );
        db.execute(
          "CREATE TABLE DriveConfig("
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "token TEXT,"
            "configid TEXT,"
            "folderid TEXT,"
            "FOREIGN KEY(folderid) REFERENCES DriveFolder(id)"
          ");"
        );
        db.execute(
          "CREATE TABLE SynchronizationConfig("
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "enabled BOOL,"
            "duration STRING"
          ");"
        );
      },
      version: 1
    );
    _initialized = true;
  }

  Future<void> purgeData() async {
    await _driveDB.close();
    await _deviceDB.close();
    String remoteDBPath = await configPathRemoteAsync;
    String deviceDBPath = await configPathDeviceAsync;
    String musicPath = callback.musicDir;
    String coverPath = callback.coverDir;
    await File(remoteDBPath).delete();
    await File(deviceDBPath).delete();
    await Directory(musicPath).delete(recursive: true);
    await Directory(coverPath).delete(recursive: true);
  }

  ///
  /// Login process
  ///

  Future<bool> driveConfigExists() async {
    final List<Map<String, dynamic>> maps = await _deviceDB.query('DriveConfig');
    return maps.length > 0;
  }

  Future<bool> driveFolderIsSet() async {
    final List<Map<String, dynamic>> maps = await _deviceDB.query('DriveFolder');
    return maps.length > 0;
  }

  Future<void> setConfig(String token, _configId, DriveFolder folder) async {
    await _deviceDB.delete('DriveConfig');
    await _deviceDB.insert(
      'DriveConfig',
      DriveConfig(token, _configId, folder.id).toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace
    );
  }

  Future<void> updateConfig(DriveConfig config) async {
    await _deviceDB.update(
        'DriveConfig',
        config.toMap(),
        where: "folderid = ?",
        whereArgs: [config.folderId]
    );
  }

  Future<void> setFolder(DriveFolder folder) async {
    await _deviceDB.delete('DriveFolder');
    await _deviceDB.insert(
      'DriveFolder',
      folder.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<DriveFolder> getFolder() async {
    final List<Map<String, dynamic>> maps = await _deviceDB.query('DriveFolder');
    return DriveFolder(maps[0]['title'], maps[0]['id'], null, null);
  }

  Future<DriveConfig> getDriveConfig() async {
    final List<Map<String, dynamic>> maps = await _deviceDB.query('DriveConfig');
    return DriveConfig(maps[0]['token'], maps[0]['configid'], maps[0]['folderid']);
  }

  Future<void> addSynchronizationConfig(SynchronizationConfig config) async {
    await _deviceDB.insert(
      'SynchronizationConfig',
      config.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<SynchronizationConfig> getSynchronizationConfig() async {
    final List<Map<String, dynamic>> maps = await _deviceDB.query('SynchronizationConfig');
    print("");
    return SynchronizationConfig(
      id: maps[0]['id'],
      enabled: maps[0]['enabled'] == 1 ? true:false,
      duration: SynchronizationConfig.parseDuration(maps[0]['duration'])
    );
  }

  Future<void> updateSynchronizationConfig(SynchronizationConfig config) async {
    _deviceDB.update(
      'SynchronizationConfig',
      config.toMap(),
      where: "id = ?",
      whereArgs: [config.id]
    );
  }

  ///
  /// Change process
  ///

  Future<bool> mergeRemoteDatabases(String path) async {
    Database tmpDB = await openDatabase(path);
    List<Map<String, dynamic>> maps = await tmpDB.query('Playlist');
    List<Playlist> remotePlaylists = List.generate(maps.length, (i) {
      return Playlist(
          id: maps[i]['id'],
          title: maps[i]['title'],
          songIds: maps[i]['songids']
      );
    });

    maps = await _driveDB.query('Playlist');
    List<Playlist> localPlaylists = List.generate(maps.length, (i) {
      return Playlist(
          id: maps[i]['id'],
          title: maps[i]['title'],
          songIds: maps[i]['songids']
      );
    });

    if (remotePlaylists.length == 0) return false;

    bool changes = false;
    List<Playlist> newPlaylist = List();
    newPlaylist.addAll(localPlaylists);

    for (Playlist remote in remotePlaylists) {
      bool contains = false;
      for (Playlist local in localPlaylists) {
        if (remote.id == local.id) {
          contains = true;
          if (remote != local) {
            newPlaylist.remove(local);
            if(local.combine(remote)) changes = true;
            newPlaylist.add(local);
          }
        }
      }
      if (!contains) {
        changes = true;
        newPlaylist.add(remote);
      }
    }
    if (changes) {
      await rebuildPlaylist(newPlaylist);
    }
    await tmpDB.close();
    return changes;
  }

  Future<void> rebuildPlaylist(List<Playlist> playlists) async {
    await _driveDB.delete('Playlist');
    for (Playlist playlist in playlists) {
      await _driveDB.insert(
        'Playlist',
        playlist.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace
      );
    }
  }

  ///
  /// Database operations
  ///

  Future<void> addSong(Song song) async {
    _deviceDB.insert(
        'Song',
        song.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace
    );
  }

  /// Adds a new album and returns the generated id.
  Future<int> addAlbum(Album album) async {
    final List<Map<String, dynamic>> query = await _deviceDB.query(
        'Album',
        where: "title = ?",
        whereArgs: [album.title]
    );

    if (query.length > 0) {
      return query.first['id'];
    }

    _deviceDB.insert(
        'Album',
        album.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace
    );
    final List<Map<String, dynamic>> insert = await _deviceDB.query(
        'Album',
        where: "title = ?",
        whereArgs: [album.title]
    );
    return insert.first['id'];
  }

  Future<void> addPlaylist(String title) async {
    var playlist = Playlist.create(title: title);
    await _driveDB.insert(
      'Playlist',
      playlist.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace
    );
  }

  Future<void> updateSong(Song song) async {
    _deviceDB.update(
      'Song',
      song.toMap(),
      where: "id = ?",
      whereArgs: [song.id]
    );
  }

  Future<void> updatePlaylist(Playlist playlist) async {
    _driveDB.update(
        'Playlist',
        playlist.toMap(),
        where: "id = ?",
        whereArgs: [playlist.id]
    );
  }

  Future<List<Song>> getSongs() async {
    final List<Map<String, dynamic>> maps = await _deviceDB.query('Song');

    return List.generate(maps.length, (i) {
      return Song(
        id: maps[i]['id'],
        title: maps[i]['title'],
        artist: maps[i]['artist'],
        duration: Duration(milliseconds: int.parse(maps[i]['duration'])),
        albumId: maps[i]['album'],
        playlistIds: maps[i]['playlists'],
        cover: maps[i]['cover'] == 1 ? true : false,
        coverPath: maps[i]['coverpath'],
        offline: maps[i]['offline'] == 1 ? true : false,
        offlinePath: maps[i]['offlinepath'],
      );
    });
  }

  Future<List<Album>> getAlbums() async {
    final List<Map<String, dynamic>> maps = await _deviceDB.query('Album');

    List<Album> albums = List.generate(maps.length, (i) {
      return Album(
        id: maps[i]['id'],
        title: maps[i]['title']
      );
    });

    for (Album album in albums) {
      final List<Map<String, dynamic>> maps = await _deviceDB.query(
        'Song',
        where: "album = ?",
        whereArgs: ['${album.id}']
      );
      List<Song> songs = List.generate(maps.length, (i) {
        return Song(
          id: maps[i]['id'],
          title: maps[i]['title'],
          artist: maps[i]['artist'],
          duration: Duration(milliseconds: int.parse(maps[i]['duration'])),
          albumId: maps[i]['album'],
          playlistIds: maps[i]['playlists'],
          cover: maps[i]['cover'] == 1 ? true : false,
          coverPath: maps[i]['coverpath'],
          offline: maps[i]['offline'] == 1 ? true : false,
          offlinePath: maps[i]['offlinepath'],
        );
      });
      album.songs = songs;
    }
    return albums;
  }

  Future<List<Playlist>> getPlaylists() async {
    final List<Map<String, dynamic>> maps = await _driveDB.query('Playlist');
    List<Playlist> playlists = List.generate(maps.length, (i) {
      return Playlist(
        id: maps[i]['id'],
        title: maps[i]['title'],
        songIds: maps[i]['songids']
      );
    });
    for (Playlist playlist in playlists) {
      List<String> songIds = playlist.getSongIds();
      if (songIds.length > 0) {
        final List<Map<String, dynamic>> songsRaw = await _deviceDB.query(
          'Song',
          where: playlist.songIdsQuery(),
          whereArgs: playlist.getSongIds()
        );
        List<Song> songs = List.generate(songsRaw.length, (i) {
          return Song(
            id: songsRaw[i]['id'],
            title: songsRaw[i]['title'],
            artist: songsRaw[i]['artist'],
            duration: Duration(milliseconds: int.parse(songsRaw[i]['duration'])),
            albumId: songsRaw[i]['album'],
            playlistIds: songsRaw[i]['playlists'],
            cover: songsRaw[i]['cover'] == 1 ? true : false,
            coverPath: songsRaw[i]['coverpath'],
            offline: songsRaw[i]['offline'] == 1 ? true : false,
            offlinePath: songsRaw[i]['offlinepath'],
          );
        });
        playlist.songs = songs;
      }
    }
    return playlists;
  }

  Future<void> deleteSong(String id) async {
    final List<Map<String, dynamic>> getAlbumId = await _deviceDB.query(
      'Song',
      where: "id = ?",
      whereArgs: [id]
    );
    bool hasAlbum = getAlbumId.length != 0 ? true : false;
    await _deviceDB.delete(
      'Song',
      where: "id = ?",
      whereArgs: [id]
    );
    if (hasAlbum) {
      int albumId = getAlbumId[0]['album'];
      int songsWithThatAlbum = Sqflite.firstIntValue(await _deviceDB.rawQuery(
          "SELECT COUNT(*) FROM Song WHERE album = $albumId"
      ));
      if (songsWithThatAlbum == 0) {

        await _deviceDB.delete(
            'Album',
            where: "id = ?",
            whereArgs: [albumId]
        );
      }
    }
  }

  Future<void> deletePlaylist(int playlistId) async {
    await _driveDB.delete(
        'Playlist',
        where: "id = ?",
        whereArgs: [playlistId]
    );
  }

  Future<bool> songExists(String id) async {
    final List<Map<String, dynamic>> query = await _deviceDB.query(
        'Song',
        where: "id = ?",
        whereArgs: [id]
    );
    return query.length == 1 ? true : false;
  }

  ///
  /// Additional config
  ///

  Future<void> reset() async {
    _driveDB = await openDatabase(_configPath);
  }

}