import 'dart:io';
import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dart_tags/dart_tags.dart';
import 'package:fplayer/formats/audio.dart';
import 'package:fplayer/formats/db.dart';
import 'package:fplayer/formats/duration.dart';

import 'package:fplayer/main.dart';


class AudioManager {
  MyAppState callback;
  AudioPlayer _player;
  bool _shuffle = false;
  Random _rand;
  bool _repeat = false;
  bool _fullScreen = false;
  Song _currentSong;
  Duration _currentSongPosition;
  Duration _currentSongLength;
  List<Song> _playlist;
  int _playlistIndex;
  AudioPlayerBar _widget;
  AudioPlayerPage _page;
  UniqueKey _widgetKey;
  UniqueKey _pageKey;
  bool _initialized = false;

  AudioManager(this.callback);

  bool get initialized => _initialized;
  AudioPlayerState get state => _player.state;

  Future<void> init() async {
    _player = AudioPlayer();
    _player.onPlayerStateChanged.listen(_onPlayerStateChanged);
    _player.onDurationChanged.listen(_onDurationChanged);
    _player.onAudioPositionChanged.listen(_onAudioPositionChanged);
    _player.onPlayerError.listen(_onPlayerError);
    _rand = Random();
    _playlist = List();
    _widgetKey = UniqueKey();
    _pageKey = UniqueKey();
    _page = AudioPlayerPage(this, key: _pageKey);
    _widget = AudioPlayerBar(this, key: _widgetKey);
    _initialized = true;
  }

  Future<void> playSong(Song song, List<Song> playlist) async {
    if (_player.state == AudioPlayerState.PLAYING) {
      await _player.release();
    }

    _playlist = playlist;
    _currentSongPosition = Duration(seconds: 0);
    _currentSongLength = Duration(seconds: 0);
    _currentSong = song;
    _playlistIndex = _playlist.indexOf(song);

    if (song.offline) {
      await _player.play(song.offlinePath, isLocal: true);
    } else {
      // Music streams are not supported at the moment because only mp3 streams
      // work with drive and music player
      try {
        String url = await callback.drive.getSongDownloadUrl(song.id);
        int result = await _player.play(url);
        print(result);
      } catch(e) {
        print(e);
      }
    }

    _widget.refresh();
  }

  bool checkIfEmpty() {
    if (_playlist.length == 0) {
      _currentSong = null;
      _stop();
      return true;
    }
    return false;
  }

  Future<void> stop() async {
    _currentSong = null;
    await _stop();
  }

  Future<void> _stop() async {
    await _player.stop();
    await _player.release();
    try { _widget.refresh(); } catch(e) {}
  }

  void _playNextSong() {
    if (!checkIfEmpty()) {
      if (!_shuffle) {
        _playlistIndex += 1;
        if (_playlistIndex >= _playlist.length) _playlistIndex = 0;
        playSong(_playlist[_playlistIndex], _playlist);
      } else
        _playRandomSong();
    }
  }

  void _playPreviousSong() {
    if (!checkIfEmpty()) {
      if (!_shuffle) {
        _playlistIndex -= 1;
        if (_playlistIndex < 0) _playlistIndex = _playlist.length - 1;
        playSong(_playlist[_playlistIndex], _playlist);
      } else
        _playRandomSong();
    }
  }

  void _playRandomSong() {
    _playlistIndex = _rand.nextInt(_playlist.length);
    playSong(_playlist[_playlistIndex], _playlist);
  }

  void pause() {
    _player.pause();
  }

  void resume() {
    _player.resume();
  }

  Future<void> _restart(Duration position) async {
    await _player.play(_currentSong.offlinePath, isLocal: true, position: position);
  }

  Future<void> goToPosition(Duration position) async {
    if (_player.state == AudioPlayerState.COMPLETED) {
      await _restart(position);
    } else await _player.seek(position);
  }

  void _toggleShuffle() {
    _shuffle = !_shuffle;
    try { _page.refresh(); } catch(e) {}
  }

  void _toggleRepeat() {
    _repeat = !_repeat;
    if (_repeat) {
      _player.setReleaseMode(ReleaseMode.LOOP);
    } else {
      _player.setReleaseMode(ReleaseMode.RELEASE);
    }
    try { _page.refresh(); } catch(e) {}
  }

  void _onPlayerStateChanged(AudioPlayerState state) {
    if (state == AudioPlayerState.COMPLETED) {
      _currentSongPosition = _currentSongLength;
      try { _page.sliderPosition(_currentSongPosition); } catch(e) {}
      if (!_repeat) _playNextSong();
    }
    _widget.refresh();
    try { _page.refresh(); } catch(e) {}
  }

  void _onDurationChanged(Duration d) {
    _currentSongLength = d;
    try { _page.duration(_currentSongLength); } catch(e) {}

  }

  void _onAudioPositionChanged(Duration d) {
    _currentSongPosition = d;
    try { _page.sliderPosition(_currentSongPosition); } catch(e) {}
    //try { _widget.refresh(); } catch(e) {}
  }

  void _onPlayerError(String msg) {
    print('audioPlayer error : $msg');
  }

  void _changeWidgetMode() {
    _fullScreen = !_fullScreen;
  }

  void resetPlayerWidget() {
    _widget.refresh();
  }

  void setWidget(AudioPlayerBar widget) {
    _widget = widget;
  }

  Widget getPlayerWidget() {
    return _widget;
  }
}


class AudioPlayerBar extends StatefulWidget {
  AudioManager _player;
  _AudioPlayerState _state;

  AudioPlayerBar(this._player, {Key key}) : super(key: key);

  void refresh() => _state.refresh();

  @override
  _AudioPlayerState createState() {
    _player._widget = this;
    _state = _AudioPlayerState(this._player);
    return _state;
  }
}

class _AudioPlayerState extends State<AudioPlayerBar> with AutomaticKeepAliveClientMixin {
  AudioManager _player;

  _AudioPlayerState(this._player);

  void refresh() async {
    if(mounted) setState(() {});
  }

  Widget _buildPlayerButton() {
    Widget playerButton;
    if (_player.state == AudioPlayerState.PLAYING) {
      playerButton = IconButton(
        iconSize: 65,
        padding: new EdgeInsets.all(0.0),
        icon: Icon(Icons.pause_circle_filled),
        color: Colors.black,
        onPressed: () {
          _player.pause();
          refresh();
        },
      );
    } else {
      playerButton = IconButton(
        iconSize: 65,
        padding: new EdgeInsets.all(0.0),
        icon: Icon(Icons.play_circle_filled),
        color: Colors.black,
        onPressed: () {
          _player.resume();
          refresh();
        },
      );
    }
    return playerButton;
  }

  Widget _buildMinimizedPlayer(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _player._changeWidgetMode();
        Navigator.push(context, MaterialPageRoute(builder: (context) => _player._page));
      },
      child: Container(
        height: 65,
        color: Colors.pink,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 65,
              height: 65,
              child: _player._currentSong.image,
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Spacer(),
                Text(
                  _player._currentSong.title,
                  style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 5,),
                Text(
                  _player._currentSong.artist,
                  style: TextStyle(color: Colors.black),
                ),
                Spacer()
              ]
            ),
            Spacer(),
            _buildPlayerButton()
          ],
        ),
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (_player._currentSong == null) return Container(height: 0, width: 0,);

    return _buildMinimizedPlayer(context);
  }

  @override
  bool get wantKeepAlive => true;

}

class AudioPlayerPage extends StatefulWidget {
  AudioManager _player;
  _AudioPlayerPageState _state;

  AudioPlayerPage(this._player, {Key key}) : super(key: key);

  void sliderPosition(Duration value) => _state.currentPosition(value);
  void duration(Duration value) => _state.duration(value);
  void refresh() => _state.refresh();

  @override
  State<StatefulWidget> createState() {
    _state = _AudioPlayerPageState(this._player);
    return _state;
  }

}

class _AudioPlayerPageState extends State<AudioPlayerPage> {
  AudioManager _player;
  Duration _currentPosition;
  Duration _duration;

  _AudioPlayerPageState(this._player);

  @override
  void initState() {
    super.initState();
    _currentPosition = _player._currentSongPosition;
    _duration = _player._currentSongLength;
  }

  void currentPosition(Duration value) {
    if (mounted) {
      setState(() {
        _currentPosition = value;
      });
    }
  }

  void duration(Duration value) {
    if (mounted) {
      setState(() {
        _duration = value;
      });
    }
  }

  void refresh() {
    if (mounted) setState(() {});
  }

  Widget _buildPlayerButton() {
    Widget playerButton;
    if (_player.state == AudioPlayerState.PLAYING) {
      playerButton = IconButton(
        iconSize: 65,
        padding: new EdgeInsets.all(0.0),
        icon: Icon(Icons.pause_circle_filled),
        color: Colors.pink,
        onPressed: () {
          _player.pause();
        },
      );
    } else {
      playerButton = IconButton(
        iconSize: 65,
        padding: new EdgeInsets.all(0.0),
        icon: Icon(Icons.play_circle_filled),
        color: Colors.pink,
        onPressed: () {
          _player.resume();
        },
      );
    }
    return playerButton;
  }

  Widget _buildShuffleButton() {
    Widget shuffleButton;
    if (_player._shuffle) {
      shuffleButton = IconButton(
        iconSize: 30,
        padding: new EdgeInsets.fromLTRB(23, 15, 22, 15),
        icon: Icon(Icons.shuffle),
        color: Colors.white,
        onPressed: _player._toggleShuffle
      );
    } else {
      shuffleButton = IconButton(
        iconSize: 30,
        padding: new EdgeInsets.fromLTRB(23, 15, 22, 15),
        icon: Icon(Icons.call_missed_outgoing),
        color: Colors.white,
          onPressed: _player._toggleShuffle
      );
    }
    return shuffleButton;
  }

  Widget _buildRepeatButton() {
    Widget repeatButton;
    if (_player._repeat) {
      repeatButton = IconButton(
          iconSize: 30,
          padding: new EdgeInsets.fromLTRB(22, 15, 23, 15),
          icon: Icon(Icons.repeat_one),
          color: Colors.white,
          onPressed: _player._toggleRepeat
      );
    } else {
      repeatButton = IconButton(
          iconSize: 30,
          padding: new EdgeInsets.fromLTRB(22, 15, 23, 15),
          icon: Icon(Icons.repeat),
          color: Colors.white,
          onPressed: _player._toggleRepeat
      );
    }
    return repeatButton;
  }

  @override
  Widget build(BuildContext context) {
    Widget slider = SliderTheme(
        data: SliderThemeData(
            trackHeight: 3.0,
            inactiveTrackColor: Colors.pink,
            activeTrackColor: Colors.pink,
            activeTickMarkColor: Colors.pink,
            thumbColor: Colors.pink
        ),
        child: Slider(
          value: _currentPosition.inSeconds.toDouble(),
          min: 0,
          max: _duration.inSeconds.toDouble(),
          onChanged: (double value) {
            _player.goToPosition(Duration(seconds: value.toInt()));
          },
        )
    );

    TextStyle timeStyle = TextStyle(color: Colors.white, fontSize: 10);

    return Scaffold(
      body: GestureDetector(
        child: Container(
          color: Colors.black,
          child: Column(
            children: <Widget>[
              SizedBox(height: 140,),
              Expanded(
                child: _player._currentSong.image,
              ),
              SizedBox(
                height: 225,
                child: Column(
                  children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(_player._currentSong.title,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                        )
                      ),
                      SizedBox(height: 5,),
                      Text(
                        _player._currentSong.artist,
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white),
                      ),
                    ],
                  ),
                  Spacer(),
                  slider,
                  SizedBox(
                    height: 10,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(DurationUtilities.printTime(_currentPosition),
                            overflow: TextOverflow.ellipsis,
                            style: timeStyle),
                          Text(DurationUtilities.printTime(_duration),
                            overflow: TextOverflow.ellipsis,
                            style: timeStyle)
                        ],
                      ),
                    )
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _buildShuffleButton(),
                      Spacer(),
                      IconButton(
                        iconSize: 65,
                        padding: new EdgeInsets.all(0.0),
                        icon: Icon(Icons.chevron_left),
                        color: Colors.pink,
                        onPressed: _player._playPreviousSong,
                      ),
                      _buildPlayerButton(),
                      IconButton(
                        iconSize: 65,
                        padding: new EdgeInsets.all(0.0),
                        icon: Icon(Icons.chevron_right),
                        color: Colors.pink,
                        onPressed: _player._playNextSong,
                      ),
                      Spacer(),
                      _buildRepeatButton()
                    ],
                  ),
                  SizedBox(height: 20)
                  ],
                ),
              )
            ],
          ),
        ),
        onPanUpdate: (details) {
          if (details.delta.dy > 0) {
            Navigator.pop(context);
          }
        },
      ),
    );
  }
}


class AudioMetaData {
  static const platform = const MethodChannel('audioMetaData');

  Future<SongMetaData> readTags(String filePath, String coverPath) async {
    try {
      List<dynamic> tags = await platform.invokeMethod(
          'readTags', {'filePath': filePath, 'coverPath': coverPath}
      );
      return SongMetaData(tags[0], tags[1], tags[2], tags[3], tags[4]);

    } on PlatformException catch(e) {
      TagProcessor tp = new TagProcessor();
      File audioFile = File(filePath);
      List<Tag> tags = await tp.getTagsFromByteArray(audioFile.readAsBytes());
      if (tags.length > 0) {
        Tag tag = tags.last;
        String title = tag.tags.containsKey("title") ?
          tag.tags["title"] : "Unknown title";
        String artist = tag.tags.containsKey("artist") ?
          tag.tags["artist"] : "Unknown artist";
        String album = tag.tags.containsKey("album") ?
          tag.tags["album"] : "Unknown album";
        String duration = tag.tags.containsKey("duration") ?
          tag.tags["duration"] : "0";
        return SongMetaData(title, artist, duration, album, false);
      }
    }
    return SongMetaData(null, null, null, null, false);
  }
}