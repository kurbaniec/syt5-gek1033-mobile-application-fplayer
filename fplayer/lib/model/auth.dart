import 'package:fplayer/main.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthManager {
  bool _initialized = false;
  GoogleSignInAccount _account;
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: <String>[
      'email',
      'https://www.googleapis.com/auth/drive'
    ],
  );
  MyAppState callback;

  AuthManager(this.callback);

  bool get initialized => _initialized;
  String get displayName => _account.displayName;

  Future<bool> isSignedIn() async {
    bool check = await _googleSignIn.isSignedIn();
    if (check) {
      // Returns null?
      // _account = await _googleSignIn.currentUser;
      // Workaround:
      //_account = await _googleSignIn.signInSilently();

    }
    return check;
  }

  Future<void> init() async {
    _account = await _googleSignIn.signInSilently();
    _initialized = true;
  }

  Future<void> login() async {
    _account = await _googleSignIn.signIn();
    bool check = (_account != null) ? true : false;
    this.callback.authChange(check);
  }

  void check() {
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      if (account != null) {
        this._account = account;
        this.callback.authChange(true);
      } else this.callback.authChange(false);
    });
  }

  Future<void> logout() async {
    await _googleSignIn.signOut();
    this.callback.authChange(false);
  }

  Future<void> logoutReset() async {
    await _googleSignIn.signOut();
  }

  GoogleSignInAccount get account => _account;

  Future<void> refreshAuthentication() async {
    _account = await _googleSignIn.signInSilently();
  }
}