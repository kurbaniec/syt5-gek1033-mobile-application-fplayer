import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fplayer/formats/db.dart';
import 'package:fplayer/formats/duration.dart';
import 'package:fplayer/main.dart';
import 'package:fplayer/other/appbar.dart';

class SettingsPage extends StatefulWidget {
  MyAppState callback;
  SettingsPage(this.callback);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<SettingsPage> {
  SynchronizationConfig _sync;

  set sync(SynchronizationConfig sync) {
    _sync = sync;
  }

  @override
  void initState() {
    super.initState();
    asyncSetup();
  }

  Future<void> asyncSetup() async {
    try {
      _sync = await widget.callback.db.getSynchronizationConfig();
      setState(() {});
    } catch(e) {}
  }

  void _syncChange(bool value) {
    _sync.enabled = value;
    widget.callback.db.updateSynchronizationConfig(_sync);
    if (_sync.enabled) {
      widget.callback.change.start();
    } else {
      widget.callback.change.stop();
    }
    setState(() {});
  }

  Future<void> _changeInterval() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        Duration tmp = Duration(seconds: _sync.duration.inSeconds);
        return AlertDialog(
          title: Text('Change synchronization interval'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                CupertinoTimerPicker(
                  initialTimerDuration: tmp,
                  mode: CupertinoTimerPickerMode.ms,
                  onTimerDurationChanged: (Duration value) {
                    tmp = value;
                  },
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Accept'),
              onPressed: () async {
                _sync.duration = tmp;
                await widget.callback.db.updateSynchronizationConfig(_sync);
                widget.callback.change.stop();
                widget.callback.change.start();
                setState(() { });
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _reset() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Reset and Logout'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("Do you want to reset the app?"),
                Text("All data will be deleted from the device"),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Accept'),
              onPressed: () async {
                await widget.callback.auth.logoutReset();
                await widget.callback.db.purgeData();
                widget.callback.reset();
                Navigator.of(context).pushReplacementNamed('/login');
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildWait() {
    return CircularProgressIndicator(
      valueColor: new AlwaysStoppedAnimation<Color>(Colors.pink),
    );
  }

  Widget _builtSettings() {
    TextStyle textStyle = TextStyle(color: Colors.white, fontSize: 16);
    TextStyle altTextStyle
      = TextStyle(color: Colors.pink, fontSize: 16, fontWeight: FontWeight.bold);

    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Synchronization",
            style: TextStyle(color: Colors.pink, fontSize: 20, fontWeight: FontWeight.bold)),
          SizedBox(height: 5,),
          MaterialButton(
            color: Colors.pink,
            child: Text("Synchronize with Drive manually", style: textStyle,),
            onPressed: () async {
              // Upload drive config
              await widget.callback.change.uploadConfig(
                  sync: _sync, enabled: true
              );
              // Get changes
              widget.callback.change.routine(null);
            },
          ),
          Row(
            children: <Widget>[
              Text("Enabled", style: textStyle,),
              Theme(
                data: Theme.of(context).copyWith(
                  unselectedWidgetColor: Colors.white,
                ),
                child: Checkbox(
                  value: _sync.enabled,
                  checkColor: Colors.white,
                  activeColor: Colors.pink,
                  focusColor: Colors.white,
                  hoverColor: Colors.white,
                  onChanged: _syncChange,
                ),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Text("Current synchronization interval   ", style: textStyle,),
              Text(DurationUtilities.printTime(_sync.duration), style: altTextStyle,),
            ],
          ),
          SizedBox(height: 5,),
          MaterialButton(
            height: 35,
            color: Colors.pink,
            child: Text("Change interval", style: textStyle,),
            onPressed: () {
              _changeInterval();
            },
          ),
          SizedBox(height: 25,),
          Text("Reset",
            style: TextStyle(color: Colors.pink, fontSize: 20, fontWeight: FontWeight.bold)),
          SizedBox(height: 5,),
          MaterialButton(
            height: 35,
            color: Colors.pink,
            child: Text("Reset and Logout", style: textStyle,),
            onPressed: () {
              _reset();
            },
          ),
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {

    Widget main;
    if (_sync == null) {
      main = _buildWait();
    } else main = _builtSettings();

    /**
     * Dark theme time picker
     * body: Theme(
        data: ThemeData(
        cupertinoOverrideTheme: CupertinoThemeData(
        textTheme: CupertinoTextThemeData(
        dateTimePickerTextStyle: TextStyle(color: Colors.white, fontSize: 16),
        pickerTextStyle: TextStyle(color: Colors.white, fontSize: 12),
        )
        )
        ),
        child: Container(
        color: Colors.black,
        alignment: Alignment.center,
        child: main
        )
        )
     */

    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context, '/home');
        return false;
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text("Settings"),
          ),
          drawer: Bar(widget.callback),
          body: Container(
              color: Colors.black,
              alignment: Alignment.center,
              child: main
          )
      )
    );
  }

}