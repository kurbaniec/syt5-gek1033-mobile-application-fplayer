## Architektur

Die Hauptbestandteile dieser verteilten Applikation sind die **lokale Datenbank**, das **Smartphone**,  die **Authentifizierung über Firebase** und die **Google Drive Cloud**. Folgende Skizze beschreibt das Konzept dahinter:

![image-20200422131420915](arch.JPG)

Das Programm greift zunächst bei Programmstart die App auf **Google Drive** des jeweiligen Benutzers zu. Die Cloud kann man nicht direkt erreichen, dazu muss man davor über **Firebase** sich **authentifizieren**. 

Bei erfolgreichem Zugriff, werden nun automatisch die Ordner auf **Drive** angezeigt am **Smartphone**. Man kann sich nun entscheiden, wo man seine Musik gespeichert hat und es bestätigen. Dabei läuft danach ein Prozess, der die ganze Musik runterlädt und verarbeitet. 

### Lokale Datenbank

Es wird für die lokale Datenbank **SQLLite** verwendet. SQLLite ist eine relationale Datenbank, die meistens in mobilen Geräten zum Einsatz kommt. Um diese Datenbank verwenden zu können, wird das Flutter-Plugin bzw. Package "*sqflite"* benötigt. Dieses Plugin muss in der **pubspec.yaml** angegeben werden:

```yaml
dependencies:
  ...
  sqflite: 
```

Danach kann man es ins Projekt einfach importieren:

```dart
import 'package:sqflite/sqflite.dart';
```



Die Lokale Datenbank speichert alle Musikdaten von der Cloud in die relationale Datenbank. Hiermit kann die App einfach dann mittels der vorhandenen Daten, alle Musiktitel anzeigen und abspielen lassen. Weiters werden die Tokens für die vergleiche (siehe [Synchronisation](sync.md)) gespeichert, die vorhandenen  Drive-Ordner und die manuelle Einstellung der Synchronisation. Die Datenbank hat 5 Tabellen: **Playlist**, **Album**, **Song**, **DriveFolder**, **DriveConfig** und **SynchronizationConfig**:

**Playlist** 

| Attributname | TYP     | PK   |
| ------------ | ------- | ---- |
| id           | INTEGER | x    |
| title        | TEXT    |      |
| songids      | TEXT    |      |

**Album**

| Attributname | TYP     | PK   |
| ------------ | ------- | ---- |
| id           | INTEGER | x    |
| title        | TEXT    |      |

**Song**

| Attributname | TYP     | PK   |
| ------------ | ------- | ---- |
| id           | TEXT    | x    |
| title        | TEXT    |      |
| artist       | TEXT    |      |
| duration     | TEXT    |      |
| album        | INTEGER |      |
| playlists    | TEXT    |      |
| cover        | BOOL    |      |
| coverpath    | TEXT    |      |
| offline      | BOOL    |      |
| offlinepath  | TEXT    |      |



### Authentifizierung

Der Benutzer muss sich mit seinem Google Drive Konto verbinden können. Dies geschieht durch die Verwendung von Firebase. Firebase ermöglicht es mittels einmaligem Login, den Benutzer in der App immer identifizieren zu können, wodurch eine Neuanmeldung nicht erforderlich ist. Die Authentifizierung ist für den Zugriff auf Google Drive notwendig. Dabei hilft das Package "*google_sign_in*". Dieses Package muss man in der **pubspec.yaml** angeben:

```yaml
dependencies:
  google_sign_in:
```

Im Projekt kann man dann das Package folgenderweise verwenden bzw. importieren:

```dart
import 'package:google_sign_in/google_sign_in.dart';
```

### Google Drive Cloud

Die Google Drive Cloud ist für den Benutzer da, um seine Musik hochzuladen. Natürlich muss der Drive Account dem fplayer-Account entsprechen, um auf die Dateien zugreifen zu können von der App aus. Diese Art von Zugriff wird mit der Kombination aus dem "*google_sign_in*" und "*googleapis*". Mit "*googleapis*" hat man die Möglichkeit die Google Drive API zu verwenden, um auf die Files auf Google Drive zuzugreifen. Dadurch kann man dann die Files, lesen, beschreiben und auch synchronisieren. 

Folgende Abhängigkeit muss man in die **pubspec.yaml** schreiben:

```yaml
...
dependencies:
  googleapis: any
  googleapis_auth: any
```

Danach kann man das Package im Projekt importieren, es wurde die Google Drive API V3 verwendet:

```dart
import 'package:googleapis/drive/v3.dart';
```



### Stateless und Statefull Widgets

#### Stateless

Ein Stateless-Widgets haben einen unveränderbaren Zustand. Das heißt, während der Laufzeit einer App kann sich dieser Zustand von dem jeweiligen Widget nicht ändern.

**Beispiel für Stateless Widgets**

```dart
import 'package:flutter/material.dart'
    
class StartScreen extends StatelessWidget{
    @override
    Widget build (BuildContext context){
        return Container(
        
        );
    }
}
```

Der Container liefert das gewünschte UI zurück und ruft dabei die überschriebene Build-Methode auf. Diese wird eben nur einmal zur Laufzeit aufgerufen und verändert nicht seinen Zustand. Um dieses Widget neu zu laden muss eine neue Instanz erstellt werden.

#### Stateful

Stateful-Widgets haben im Gegensatz einen veränderbaren Zustand, diese können öfters in der UI aufgerufen werden während der Laufzeit. Folgenden Aufbau hat ein Stateful-Widget:

```dart
import 'package:flutter/material.dart'
    
class StartScreen extends StatefulWidget{
    @override
    _StartScreenState createState() => _StartScreenState()
}
    
class StartScreen extends State<StartScreen>{
    @override
    Widget build (BuildContext context){
        return Container(
        
        );
    }
}
```



Wie gewohnt wird das Widget gebuildet, aber dieses mal kann es mehrmals erstellt werden, da die build-Methode überschrieben wurde. Mit der Methode `setState()` kann man ein Neuladen des Widgets auslösen [^1]



### Navigator in Flutter

In Flutter kann man einfach mit `Navigator` zwischen verschiedenen Screens in einer App wechseln. Als Beispiel wurden 2 Widgets erstellt und das erste mit `Navigator.push()`erreicht. Mit der Methode `Navigator.pop()` gelangt man zurück zum ersten Widget

```dart
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: FirstRoute(),
  ));
}

class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Route'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Open route'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SecondRoute()),
            );
          },
        ),
      ),
    );
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}
```

 [^2] 

###  Quellen

[^1]: [Stateless vs Stateful](https://medium.com/flutter-community/flutter-stateful-vs-stateless-db325309deae)
[^2]: [Navigator in Flutter](https://flutter.dev/docs/cookbook/navigation/navigation-basics)



