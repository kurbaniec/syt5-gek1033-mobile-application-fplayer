## Synchronisation

Die Synchronisation geschieht durch den Einsatz von Tokens die man vergleicht. Bei herunterladen der aktuellen Musik von Drive, wird der Token ausgelesen. Bei jeder Änderung am Drive wird sozusagen ein neuer Token erstellt, dieser wird mit dem lokal gespeicherten Token verglichen. Bei einer Ungleichheit ist eine Änderung am Drive geschehen. Die Überprüfung erfolgt in der **change.dart** Klasse mittels der `routine()` Methode. Diese Klasse wird als ChangeManager initialisiert in der **main.dart**. Der periodische Timer wird mittels `timer.periodic` gestartet und ruft die Funktion `routine()` auf.

Der Token wird immer auch in der lokalen Datenbank abgespeichert.

Es wird die Methode  `start()` ausgeführt ausgeführt, diese holt sich aus der Datenbank die aktuellen Konfigurationen (Token ID, Songs,....). 

```dart
 Future<void> start() async {
    if (callback.db.initialized) {
      SynchronizationConfig config = await callback.db.getSynchronizationConfig();
      if (_routineTimer != null) stop();
      _routineTimer = Timer.periodic(config.duration, routine);
    }
  }
```

Die Methode `routine()` überprüft zunächst ob das Gerät mit allen Services verbunden ist und online ist. Damit dann die eigentliche Synchronisation gestartet werden kann

```dart
 if (callback.initialized
        && callback.network == Network.online && !_routineWorking
    ) {
```

Es wird der aktuelle Token abgerufen aus der lokalen Datenbank, damit dieser später verglichen werden kann mit dem Token vom Drive.

```dart
 DriveConfig config = await callback.db.getDriveConfig();
 String token = config.token;
```

Ein Beispiel einer Überprüfung ist die Überprüfung ob die Player-Konfiguration verändert bzw. gelöscht wurde. Es werden zunächst die Changes in eine ChangeList (Mit dieser Liste werden die Veränderungen vom jeweiligen Objekt abgebildet) eingetragen. Diese werden dann überprüft, ob das File gelöscht wurde. Danach wird noch bestimmt ob die Veränderung Lokal oder Online war. Entweder wird das File hochgeladen oder heruntergeladen.

```dart
drive.ChangeList changes = await callback.drive.getChanges(
              token);
          for (drive.Change change in changes.changes) {
            // Check if player configuration got changed
            if (change.fileId == callback.configId) {
              // Check if it was removed or trashed
              if (change.removed == true ||
                  (change.file != null && change.file.trashed)) {
                // Upload configuration and update device config
                String configId = await callback.drive.uploadFile(
                    callback.folder.id, File(callback.db.configPath)
                );
                DriveConfig config = await callback.db.getDriveConfig();
                await callback.db.setConfig(config.token, configId, callback.folder);
              } else {
                // Remote file was changed
                // Combine local and remote database
                String tmpPath = await DBManager.configPathTemporaryAsync;
                await callback.drive.downloadFile(change.fileId, tmpPath);
                bool difference = await callback.db.mergeRemoteDatabases(tmpPath);
            
                await File(tmpPath).delete();
              }
            }
```





