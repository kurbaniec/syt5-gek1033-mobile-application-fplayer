# Userstory List

| ID   | Userstory                                                    | SP   | Head | Priority |
| ---- | ------------------------------------------------------------ | ---- | ---- | -------- |
| 1    | Als Benutzer möchte ich Musik hochladen, damit diese später gestreamt werden kann. | 3    | SHED | MUST     |
| 2    | Als Benutzer möchte ich Musik per Stream abspielen           | 3    | KURB | MUST     |
| 3    | Als Benutzer möchte ich Musik offline abspielen              | 5    | SHED | MUST     |
| 4    | Als Benutzer möchte ich Metadaten von Liedern ändern         | 8    | SHED | NICE     |
| 5    | Als Benutzer möchte ich Lieder mittels Alben kategorisieren  | 8    | SHED | SHOULD   |
| 6    | Als Benutzer möchte ich Lieder mittels Playlists kategorisieren | 5    | SHED | MUST     |
| 7    | Als Benutzer möchte ich Lieder zufällig abspielen            | 5    | SHED | NICE     |
| 8    | Als Benutzer möchte ich Lieder aus meiner Cloud hinzufügen   | 13   | KURB | MUST     |
| 9    | Als Benutzer möchte ich Lieder per Equalizer manipulieren können | 8    | SHED | NICE     |
| 10   | Als Bentuzer möchte ich den Lautstärkepegel unabhängig vom System verändern können | 8    | SHED | NICE     |
| 11   | Als Benutzer möchte ich mehrere Cloudanbieter verwenden können | 21   | KURB | NICE     |
| 12   | Als Benutzer möchte ich Musik offline von meiner Cloud abspeichern können | 13   | KURB | NICE     |